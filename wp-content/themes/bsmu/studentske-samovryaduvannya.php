<div class="container row_administration">

    <div class="row row_assoc">

        <div class="col-md-9">

            <?php $the_content = get_the_content(); ?>

            <?php if ($the_content): ?>

                <div class="row row_assoc">
                    <div class="col-md-12">
                        <div class="senate_inf memb_assoc st_gv">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>

            <?php endif; ?>

            <?php $block_header_card_info = get_field('block_header_card_info'); ?>

            <?php if ($block_header_card_info): ?>

                <?php foreach (($block_header_card_info) as $block_header_card_info): ?>

                    <div class="row">

                        <?php $header_info_card = $block_header_card_info['header_info']; ?>

                        <?php if ($header_info_card): ?>
                            <h5 class="header_info"><?php echo $header_info_card; ?></h5>
                        <?php endif; ?>


                        <?php $info_users = $block_header_card_info['info_user']; ?>
                        <!-- Items -->
                        <?php
                        foreach (($info_users) as $info_user) {
                            ?>

                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <div class="box_style_1 <?php if (is_page('2170')): ?>small<?php endif; ?>">

                                    <!-- Photo -->
                                    <?php $photo_user = $info_user['photo']; ?>
                                    <?php if ($photo_user): ?>
                                        <p><img src="<?php echo $photo_user; ?>" class="img-circle styled"
                                                alt=""/></p>
                                    <?php else: ?>
                                        <p>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/noavatar1.png"
                                                 class="img-circle styled" alt=""/>
                                        </p>
                                    <?php endif; ?>

                                    <!-- User name and position -->
                                    <h4><?php echo $info_user['username']; ?>
                                        <?php $position_user = $info_user['position']; ?>
                                        <?php if ($position_user): ?>
                                            <p>
                                                <small><?php echo $position_user; ?></small>
                                            </p>
                                        <?php endif; ?>
                                    </h4>

                                    <!-- Email -->
                                    <?php $mail_user = $info_user['email']; ?>
                                    <?php $telephone_user = $info_user['telephone_number']; ?>
                                    <?php $telephone_user_link = $info_user['telephone_number_link']; ?>
                                    <div class="cardinfo-number-email-block">

                                        <?php if ($telephone_user_link): ?>
                                            <a href="tel:<?php echo $telephone_user_link; ?>">
                                                <i class="fa fa-phone"
                                                   aria-hidden="true"></i> <?php echo $telephone_user; ?>
                                            </a>
                                        <?php endif; ?>

                                        <?php if ($mail_user): ?>
                                            <a href="mailto:<?php echo $mail_user; ?>">
                                                <i class="fa fa-envelope"
                                                   aria-hidden="true"></i> <?php echo $mail_user; ?></a>
                                        <?php endif; ?>

                                    </div>

                                    <ul class="social_team">

                                        <!-- Facebook -->
                                        <?php $facebook_user = $info_user['facebook']; ?>
                                        <?php if ($facebook_user): ?>
                                            <li><a target="_blank" href="<?php echo $facebook_user; ?>"><i
                                                            class="icon-facebook"></i></a></li>
                                        <?php endif; ?>

                                        <!-- Twitter -->
                                        <?php $twitter_user = $info_user['twitter']; ?>
                                        <?php if ($twitter_user): ?>
                                            <li><a target="_blank" href="<?php echo $twitter_user; ?>"><i
                                                            class="icon-twitter"></i></a></li>
                                        <?php endif; ?>

                                        <!-- Google+ -->
                                        <?php $google_user = $info_user['google']; ?>
                                        <?php if ($google_user): ?>
                                            <li><a target="_blank" href="<?php echo $google_user; ?>"><i
                                                            class="icon-google"></i></a></li>
                                        <?php endif; ?>

                                    </ul>

                                </div>

                            </div>

                            <?php
                        }
                        ?>

                    </div>

                <?php endforeach; ?>

            <?php endif; ?>

            <?php if (get_field('outer_link')): ?>

                <div class="outer_link_block center">
                    <br>
                    <hr>
                    <br>
                    <a href="<?php the_field('outer_link'); ?>" target="_blank"
                       class="outer_link"><?php pll_e('Перейти на сайт'); ?></a>
                </div>

            <?php endif; ?>

        </div>

        <div class="col-md-3 event_bl sidebar_events_news">

            <!-- Include Sidebar -->
            <?php get_template_part('sidebar'); ?>

        </div>

    </div>
</div>