<div class="memb_assoc act_internal">

    <!-- General information -->
    <p><?php echo get_field('international_general_info'); ?></p>

    <p><strong>1. <?php pll_e('Угоди про співробітництво'); ?></strong></p>

    <?php $table_agreements = get_field('table_international_agreements'); ?>
    <?php if ($table_agreements): ?>

        <div class="scrollable">

            <div>

                <table border="1" cellspacing="0" cellpadding="0">

                    <tr>
                        <td><?php pll_e('Країна'); ?></td>
                        <td><?php pll_e('Навчальний заклад або його підрозділи, з якими підписано угоду про співробітництво'); ?></td>
                        <td><?php pll_e('Дата підписання угоди'); ?></td>
                        <td><?php pll_e('Головний напрямок співробітництва'); ?></td>
                    </tr>

                    <?php
                    foreach (($table_agreements) as $table_international_agreements) {
                        ?>

                        <!-- List of the International agreements -->
                        <tr>
                            <td><?php echo $table_international_agreements['country']; ?></td>
                            <td><?php echo $table_international_agreements['agree_educ']; ?></td>
                            <td><?php echo $table_international_agreements['date']; ?></td>
                            <td><?php echo $table_international_agreements['direction_of_cooperation']; ?></td>
                        </tr>

                        <?php
                    }
                    ?>

                </table>

            </div>

        </div>

    <?php endif; ?>

    <!-- Carousel images -->
    <?php $carousel_agreements = get_field('photoes_international_agreements'); ?>
    <?php if ($carousel_agreements): ?>

        <div class="carousel">

            <div class="carousel-viewport">

                <ul class="carousel-list">

                    <?php
                    foreach (($carousel_agreements) as $photoes_international_agreements) {
                        ?>

                        <li class="carousel-item"><img
                                    src="<?php echo $photoes_international_agreements['photo']; ?>"
                                    alt=""/></li>

                        <?php
                    }
                    ?>

                </ul>

                <a class="carousel-nav carousel-nav_prev js-carousel-nav_prev"><span
                            class="arrow arrow_left"></span></a>

                <a class="carousel-nav carousel-nav_next js-carousel-nav_next"><span
                            class="arrow arrow_right"></span></a>

                <nav class="carousel-bulletNav"></nav>

            </div>

        </div>

    <?php endif; ?>

    <?php $internationals_cathedras = get_field('international_cathedras'); ?>
    <?php if ($internationals_cathedras): ?>

        <p>
            <strong>2. <?php pll_e('Проведення освітніх і наукових обмінів, спільних наукових досліджень, стажування та навчання за кордоном науково-педагогічних кадрів та студентів'); ?>
                .</strong></p>

        <!-- Content -->
        <p><?php echo get_field('exchanges_by_studentss'); ?></p>

        <ol>

            <?php
            foreach (($internationals_cathedras) as $international_cathedras) {
                ?>

                <!-- List of the Internationals_cathedras -->
                <li><a target="_blank"
                       href="<?php echo $international_cathedras['link']; ?>"><?php echo $international_cathedras['name']; ?></a> <?php echo $international_cathedras['description']; ?>
                </li>

                <?php
            }
            ?>

        </ol>

    <?php endif; ?>

    <!-- Images -->
    <?php $mix_photoes = get_field('mix_photoes'); ?>
    <?php if ($mix_photoes): ?>

        <div class="row mix_images">

            <div class="col-md-12">

                <?php
                foreach (($mix_photoes) as $mix_photoes) {
                    ?>

                    <div class="col-md-6">
                        <img src="<?php echo $mix_photoes['imgs']; ?>" alt=""/>
                    </div>

                    <?php
                }
                ?>

            </div>

        </div>

    <?php endif; ?>

    <p><strong>3. <?php pll_e('Участь у міжнародних конференціях, семінарах, фестивалях, виставках в Україні та за кордоном'); ?>.</strong></p>

    <!-- Content -->
    <p><?php echo get_field('participation_conferences'); ?></p>

    <ul>

        <?php
        foreach (get_field('repeater_list') as $repeater_list) {
            ?>

            <!-- List of the Participation_conferences -->
            <li><?php echo $repeater_list['elements_of_list']; ?></li>

            <?php
        }
        ?>

    </ul>

    <!-- Images -->
    <?php $pct_mix = get_field('images_lists'); ?>
    <?php if ($pct_mix): ?>

        <div class="row mix_images">

            <div class="col-md-12">

                <?php
                foreach (($pct_mix) as $images_lists) {
                    ?>

                    <div class="col-md-6">
                        <img src="<?php echo $images_lists['phts']; ?>" alt=""/>
                    </div>

                    <?php
                }
                ?>

            </div>

        </div>

    <?php endif; ?>

    <!-- Content -->
    <p><?php echo get_field('content_conferences'); ?></p>

    <p><strong>4. <?php pll_e('Інші види міжнародної діяльності'); ?>.</strong></p>

    <!-- Content -->
    <p><?php echo get_field('others_international'); ?></p>

    <!-- Images -->
    <div class="row mix_images">

        <div class="col-md-12">

            <?php
            foreach (get_field('photoes_others_international') as $photoes_others_international) {
                ?>

                <div class="col-md-6">
                    <img src="<?php echo $photoes_others_international['photo']; ?>" alt=""/>
                </div>

                <?php
            }
            ?>

        </div>

    </div>

    <?php $ot_pointers = get_field('others_pointers'); ?>
    <?php if ($ot_pointers): ?>

        <?php
        foreach (($ot_pointers) as $others_pointers) {
            ?>

            <!-- Name -->
            <p><strong><?php echo $others_pointers['number_and_name']; ?></strong></p>

            <!-- Content -->
            <p><?php echo $others_pointers['content']; ?></p>

            <!-- Images -->
            <?php $photo0 = $others_pointers['photoes_rep']; ?>
            <?php if ($photo0): ?>

                <div class="row mix_images">

                    <div class="col-md-12">

                        <?php
                        foreach (($photo0) as $photoes_rep) {
                            ?>

                            <div class="col-md-6">
                                <img src="<?php echo $photoes_rep['photo']; ?>" alt=""/>
                            </div>

                            <?php
                        }
                        ?>

                    </div>

                </div>

            <?php endif; ?>

            <?php
        }
        ?>

    <?php endif; ?>

</div>