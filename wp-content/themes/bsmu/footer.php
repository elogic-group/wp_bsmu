<!-- Footer -->
<footer>

    <div class="container">

        <div class="row">

            <div class="col-md-9">

                <!-- Top footer menu -->
                <?php wp_nav_menu('menu=top_footer_menu&depth=2'); ?>

            </div>

            <div class="col-md-3">

                <div class="contact_footer">

                    <h4><?php pll_e('Контакти'); ?></h4>

                    <ul>

                        <!-- Email field -->
                        <?php $mail = get_option('option_email'); ?>
                        <?php if ($mail): ?>
                            <li>Email: <a title="<?php echo $mail; ?>" target="_blank"
                                          href="<?php echo $mail; ?>"><?php echo $mail; ?></a></li>
                        <?php endif; ?>

                        <!-- Phone field -->
                        <?php $phone = get_option('option_phone'); ?>
                        <?php if ($phone): ?>
                            <li><?php pll_e('Телефон'); ?>: <a href="<?php echo $phone; ?>"><?php echo $phone; ?></a>
                            </li>
                        <?php endif; ?>

                        <!-- Address field -->
                        <?php $address = get_option('option_address'); ?>
                        <?php if ($address): ?>
                            <li><?php pll_e('Адреса'); ?>: <?php echo $address; ?></li>
                        <?php endif; ?>

                    </ul>

                </div>

            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-md-9">

                <!-- Bottom footer menu -->
                <?php wp_nav_menu('menu=bottom_footer_menu&depth=2'); ?>

            </div>

            <div class="col-md-3">

                <div class="line-footer">

                    <h4><?php pll_e('Підписатись'); ?></h4>

                    <!-- Change attributes fields of form -->
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            $("#es_txt_email_pg").attr('placeholder', 'Email');
                            $("#es_txt_button_pg").attr('value', '');
                        });
                    </script>

                    <!-- Subscribe form -->
                    <?php echo do_shortcode('[email-subscribers namefield="NO" desc="" group="Public"] '); ?>

                </div>

                <div class="line-footer">

                    <ul class="social_net">

                        <!-- Facebook -->
                        <?php $facebook = get_option('option_facebook'); ?>
                        <?php if ($facebook): ?>
                            <li><a target="_blank" href="<?php echo $facebook; ?>"><i class="fab fa-facebook-f"></i></a>
                            </li>
                        <?php endif; ?>

                        <!-- Twitter -->
                        <?php $twitter = get_option('option_twitter'); ?>
                        <?php if ($twitter): ?>
                            <li><a target="_blank" href="<?php echo $twitter; ?>"><i class="fab fa-twitter"></i></a>
                            </li>
                        <?php endif; ?>

                        <!-- Telegram -->
                        <?php $telegram = get_option('option_telegram'); ?>
                        <?php if ($telegram): ?>
                            <li><a target="_blank" href="<?php echo $telegram; ?>"><i class="fab fa-telegram-plane"></i></a>
                            </li>
                        <?php endif; ?>

                        <!-- Youtube -->
                        <?php $youtube = get_option('option_youtube'); ?>
                        <?php if ($youtube): ?>
                            <li><a target="_blank" href="<?php echo $youtube; ?>"><i class="fab fa-youtube"></i></a>
                            </li>
                        <?php endif; ?>

                    </ul>

                </div>

            </div>

        </div>

    </div>

</footer>


<span class="scrollup" id="scrollup"></span>
<!-- Scripts -->
<script type="text/javascript">
    new WOW().init();
</script>

<script type="text/javascript"
        src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript"
        src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.fancybox-media.js"></script>
<script type="text/javascript"
        src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.stellar.js"></script>

<?php wp_footer(); ?>

</body>

</html>
