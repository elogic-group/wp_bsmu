<?php
/*
  Template Name: Шаблон для "Особиста сторінка"
*/
?>

    <!-- Include Header -->
<?php get_header(); ?>

    <!-- User Page -->
    <div class="bg_page">

        <div class="container">

            <div class="row row_rector">

                <div class="col-md-12">

                    <div class="rector">

                        <!-- Photo -->
                        <img src="<?php echo get_field('userphoto'); ?>" alt=""/>

                        <!-- User name and general information -->
                        <strong><?php echo get_field('username'); ?></strong><?php echo get_field('generalluser_info'); ?>

                        <!-- Education ... -->
                        <?php $person_education = get_field('education_ed'); ?>
                        <?php if ($person_education): ?>
                    <?php
/*
  Template Name: Шаблон для "Особиста сторінка"
*/
?>

    <!-- Include Header -->
<?php get_header(); ?>

    <!-- User Page -->
    <div class="bg_page">

        <div class="container">

            <div class="row row_rector">

                <div class="col-md-12">

                    <div class="rector">

                        <!-- Photo -->
                        <img src="<?php echo get_field('userphoto'); ?>" alt=""/>

                        <!-- User name and general information -->
                        <strong><?php echo get_field('username'); ?></strong><?php echo get_field('generalluser_info'); ?>

                        <!-- Education ... -->
                        <?php $person_education = get_field('education_ed'); ?>
                        <?php if ($person_education): ?>
                            <h4><?php pll_e('Освіта, наукові ступені і звання'); ?></h4>
                            <p><?php echo $person_education; ?></p>
                        <?php endif; ?>

                        <!-- Career -->
                        <?php $career = get_field('careeruser'); ?>
                        <?php if ($career): ?>
                            <h4><?php pll_e('Кар\'єра'); ?></h4>
                            <p><?php echo $career; ?></p>
                        <?php endif; ?>

                        <!-- Scientific interests -->
                        <?php $scientific_interests = get_field('scienceinterest'); ?>
                        <?php if ($scientific_interests): ?>
                            <h4><?php pll_e('Наукові інтереси'); ?></h4>
                            <?php echo $scientific_interests; ?>
                        <?php endif; ?>

                        <!-- Honors -->
                        <?php $grands = get_field('lawers'); ?>
                        <?php if ($grands): ?>
                            <h4><?php pll_e('Відзнаки'); ?>:</h4>
                            <ul>
                                <?php
                                foreach (($grands) as $lawers) {
                                    ?>
                                    <li><?php echo $lawers['all_lawers']; ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        <?php endif; ?>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Include Footer -->
<?php get_footer(); ?>        <h4><?php pll_e('Освіта, наукові ступені і звання'); ?></h4>
                            <p><?php echo $person_education; ?></p>
                        <?php endif; ?>

                        <!-- Career -->
                        <?php $career = get_field('careeruser'); ?>
                        <?php if ($career): ?>
                            <h4><?php pll_e('Кар\'єра'); ?></h4>
                            <p><?php echo $career; ?></p>
                        <?php endif; ?>

                        <!-- Scientific interests -->
                        <?php $scientific_interests = get_field('scienceinterest'); ?>
                        <?php if ($scientific_interests): ?>
                            <h4><?php pll_e('Наукові інтереси'); ?></h4>
                            <?php echo $scientific_interests; ?>
                        <?php endif; ?>

                        <!-- Honors -->
                        <?php $grands = get_field('lawers'); ?>
                        <?php if ($grands): ?>
                            <h4><?php pll_e('Відзнаки'); ?>:</h4>
                            <ul>
                                <?php
                                foreach (($grands) as $lawers) {
                                    ?>
                                    <li><?php echo $lawers['all_lawers']; ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        <?php endif; ?>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Include Footer -->
<?php get_footer(); ?>