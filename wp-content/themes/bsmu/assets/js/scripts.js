$(document).ready(function ($) {


    /* Scroll to top start */

    $(window).scroll(function (){
        if ($(this).scrollTop() > 700){
            $("#scrollup").fadeIn(500);
        } else{
            $("#scrollup").fadeOut(500);
        }
    });

    $("#scrollup").click(function (){
        $("body,html").animate({
            scrollTop:0
        }, 800);
        return false;
    });

    /* Scroll to top end */


    /* Home Slider */
    if($('.main-slider.default-style .tp-banner').length){
        jQuery('.main-slider.default-style .tp-banner').show().revolution({
            delay:10000,
            startwidth:1200,
            startheight:720,
            hideThumbs:720,
            thumbWidth:80,
            thumbHeight:50,
            thumbAmount:5,
            navigationType:"bullet",
            navigationArrows:"0",
            navigationStyle:"preview3",
            touchenabled:"on",
            onHoverStop:"on",
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,
            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
            keyboardNavigation:"off",
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:40,
            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,
            shadow:0,
            fullWidth:"on",
            fullScreen:"off",
            spinner:"spinner4",
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",
            autoHeight:"off",
            forceFullWidth:"on",
            hideThumbsOnMobile:"on",
            hideNavDelayOnMobile:1500,
            hideBulletsOnMobile:"on",
            hideArrowsOnMobile:"on",
            hideThumbsUnderResolution:0,
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0,
            videoJsPath:"",
            fullScreenOffsetContainer: ""
        });
    }


    /* Sticky menu */


    if($(window).width() > 1024 ) {
        var $menu = $("#menu");

        $(window).scroll(function(){
            if ( $(this).scrollTop() > 149 && $menu.hasClass("default") ){
                $('body').css({'padding-top':'72px'});
                $menu.removeClass("default").addClass("fixed");
            } else if($(this).scrollTop() <= 149 && $menu.hasClass("fixed")) {
                $('body').css({'padding-top':'0px'});
                $menu.removeClass("fixed").addClass("default");
            }
        });
    }



    $('.k2img').each(function(){
        if ( $(this).attr('src').indexOf('.JPG') > 0 && ($(this).context.naturalWidth == 0 || $(this).readyState == 'uninitialized') ){
            var src = $(this).attr('src').split('.JPG')[0] + '.jpg';
            $(this).attr('src', src);
            $(this).closest('a.fancybox').attr('href', src);

            $(this).on('error', function(){
                if($(this).context.naturalWidth == 0 || $(this).readyState == 'uninitialized'){
                    $(this).attr('src', site_url + '/wp-content/uploads/2017/11/news-default.jpg');
                }
            });
        }
    });


    /*Одинаковая высота для элементов (высота самого большего) start */

    if($(window).width() > 991 ) {
        $.fn.equivalent = function (){
            var $blocks = $(this),
                maxH    = $blocks.eq(0).outerHeight();
            $blocks.each(function(){
                maxH = ( $(this).outerHeight() > maxH ) ? $(this).outerHeight() : maxH;
            });
            $blocks.css("min-height",maxH);
        }
        $('.cs-event-area .cs-event-col').equivalent();
    }

    /*Одинаковая высота для элементов (высота самого большего) end */


    //Mobile menu start

    $('.header-mobile-wrapper .mobile-nav-btn').on('click', function () {
       $('.header-mobile-wrapper .header-nav-mob').stop().slideToggle(200);
    });

    if($(window).width() < 1025 ) {

        $('nav.nav_wrap').find('li.menu-item-has-children').append('<span class="arrow-btn"><i class="fas fa-angle-down"></i></span>');

        var ArrowBtn = $('li.menu-item').find('span.arrow-btn');

        $(ArrowBtn, this).on('click', function () {
            $(this).toggleClass('active');
            $(this).parent().toggleClass('current-parent-element').children('ul.sub-menu', this).stop().slideToggle(200);
        });

        var TriggerSearchBtn = $('#searchform .box .trigger-form-mobile');

        $(TriggerSearchBtn).on('click', function () {
            $(this).prev('.container-3').fadeIn(300);
        });

        $(document).mouseup(function (e) {
            var SearchContainer = $("#searchform .box .container-3");
            if (SearchContainer.has(e.target).length === 0){
                $(SearchContainer).fadeOut(300);
            }
        });

    }

    //Mobile menu end

});