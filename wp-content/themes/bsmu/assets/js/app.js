// $(document).ready(function() {
//     $(".menu_general_all [href]").each(function() {
//         if (this.href == window.location.href) {
//             $(this).addClass("active");
//         }
//     });
// });



/* Slider on History of university page */
(function($) {

    $(function() {
        Carousel.init();
    });

    Carousel = {
        init: function() {
            this.$container = $('.carousel');
            this.$triggerNext = this.$container.find('.js-carousel-nav_next');
            this.$triggerPrev = this.$container.find('.js-carousel-nav_prev');
            this.$carousel = $('.carousel-list');
            this.$carouselListItem = this.$carousel.find('li');
            this.$carouselListItemFirst = this.$carousel.find('li:first');
            this.$carouselListItemLast = this.$carousel.find('li:last');
            this.$carouselItem = $('.carousel-item');
            this.$selectedItem = this.$carousel.find('.carousel-item_isSelected');

            this.marker = this.$selectedItem.index();
            this.paddedMarker = this.$selectedItem.index() + 1;
            this.viewportWidth = this.$carouselItem.width();
            this.carouselItemLength = this.$carouselItem.length;
            this.theEnd = this.carouselItemLength - 1;

            this.bulletNav();
            this.bindEvents();
        },

        bindEvents: function() {
            var self = this;
            var $triggerBullet = this.$container.find('.js-carousel-bulletNav-control');
            //var indexBullet = $triggerBullet.index();

            this.$triggerNext.on('click', function(e) {
                e.preventDefault();
                self.nextSlide();
            });

            this.$triggerPrev.on('click', function(e) {
                e.preventDefault();
                self.prevSlide();
            });

            $triggerBullet.on('click', function(e, i) {
                var $this = $(this);
                var i = $this.index();

                e.preventDefault();
                self.bulletNavControl(i);
            });
        },

        //Return the value to slide container by
        slideBy: function() {
            var slideBy = (this.viewportWidth * -this.marker) + 'px';

            return slideBy;
        },

        updateMarkerNext: function() {
            var $currentActive = this.$carousel.find('.carousel-item_isSelected');

            $currentActive.removeClass('carousel-item_isSelected');
            $currentActive.next().addClass('carousel-item_isSelected');
        },

        updateMarkerPrev: function() {
            var $currentActive = this.$carousel.find('.carousel-item_isSelected');

            $currentActive.removeClass('carousel-item_isSelected');
            $currentActive.prev().addClass('carousel-item_isSelected');
        },

        updateMarkerToFirst: function() {
            this.$carouselListItemLast.removeClass('carousel-item_isSelected');
            this.$carouselListItemFirst.addClass('carousel-item_isSelected');
        },

        updateMarkerToLast: function() {
            this.$carouselListItemFirst.removeClass('carousel-item_isSelected');
            this.$carouselListItemLast.addClass('carousel-item_isSelected');
        },

        incrementSlide: function() {
            if (this.marker < this.carouselItemLength) {
                this.marker++;
            } else {
                this.marker = 1;
            }
        },

        decrementSlide: function() {
            if (this.marker == 0 ) {
                this.marker = this.theEnd;
            } else {
                this.marker--;
            }
        },

        // Go to next slide
        nextSlide: function() {
            this.incrementSlide();

            if (this.marker < this.carouselItemLength) {
                this.updateMarkerNext();
                this.$carousel.css('margin-left', this.slideBy());
            } else {
                this.updateMarkerToFirst();
                this.$carousel.css('margin-left', 0);
            }
        },

        // Go to previous slide
        prevSlide: function() {
            this.decrementSlide();

            if (this.marker == this.theEnd) {
                this.updateMarkerToLast();
                this.$carousel.css('margin-left', this.slideBy());
            } else {
                this.updateMarkerPrev();
                this.$carousel.css('margin-left', this.slideBy());
            }

        },

        bulletNav: function() {
            var $bulletNav = $('.carousel-bulletNav');

            for(i = 0; i < this.carouselItemLength; i++) {
                $bulletNav.append('<li class="carousel-bulletNav-control js-carousel-bulletNav-control"><a></a></li>');
            }
        },

        updateMarkerBullet: function(index) {
            var $currentActive = this.$carousel.find('.carousel-item_isSelected');

            $currentActive.removeClass('carousel-item_isSelected');
            this.$carouselItem.eq(index).addClass('carousel-item_isSelected');
        },

        bulletNavControl: function(index) {
            var slideByBullet = (this.viewportWidth * -index) + 'px';

            this.$carousel.css('margin-left', slideByBullet);
            this.updateMarkerBullet(index);
            this.marker = index;
        }
    }

}(jQuery));

/* Counter */
jQuery(document).ready(function() {
    $('.counter').each(function() {
        var $this = $(this),
            countTo = $this.attr('data-count');
        $({ countNum: $this.text()}).animate({
                countNum: countTo
            },
            {
                duration: 2000,
                easing:'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                }
            });
    });

    // The function toggles more (hidden) text when the user clicks on "Read more". The IF ELSE statement ensures that the text 'read more' and 'read less' changes interchangeably when clicked on.
    /*$('.moreless-button').click(function() {
        $('.course_list_desc').css('height', 'auto');
        $('.moretext').slideToggle();
        if ($('.moreless-button').text() == "Приховати") {
            $('.course_list_desc').css('height', '220px');
            $(this).text("Детальніше")
        } else {
            $(this).text("Приховати");
        }
    });



    $('.moreless-button1').click(function() {
        $('.course_list_desc1').css('height', 'auto');
        $('.moretext1').slideToggle();
        if ($('.moreless-button1').text() == "Приховати") {
            $('.course_list_desc1').css('height', '220px');
            $(this).text("Детальніше")
        } else {
            $(this).text("Приховати");
        }
    });

    $('.moreless-button2').click(function() {
        $('.course_list_desc2').css('height', 'auto');
        $('.moretext2').slideToggle();
        if ($('.moreless-button2').text() == "Приховати") {
            $('.course_list_desc2').css('height', '220px');
            $(this).text("Детальніше")
        } else {
            $(this).text("Приховати");
        }
    });

    $('.moreless-button3').click(function() {
        $('.course_list_desc3').css('height', 'auto');
        $('.moretext3').slideToggle();
        if ($('.moreless-button3').text() == "Приховати") {
            $('.course_list_desc3').css('height', '220px');
            $(this).text("Детальніше")
        } else {
            $(this).text("Приховати");
        }
    });*/

    $(document).ready(function(){

        //global variables
        var $timer = 0;
        var $counter;

        function expand(e) {
            $timer += 150;

            if ($timer == 450){
                clearInterval($counter);
                $timer = 0;
                e.addClass('expand');
            }
        }

        $('#deepGallery ul li a').hover(function(){
            var $e = $(this);
            $counter = setInterval(function() {
                expand($e);
            }, 150);
        }, function() {
            clearInterval($counter);
            $timer = 0;
            $(this).removeClass('expand');
        });
    });

    /*/!* Sticky menu *!/
    $(document).ready(function(){

        var $menu = $("#menu");

        $(window).scroll(function(){
            if ( $(this).scrollTop() > 100 && $menu.hasClass("default") ){
                $menu.removeClass("default").addClass("fixed");
            } else if($(this).scrollTop() <= 100 && $menu.hasClass("fixed")) {
                $menu.removeClass("fixed").addClass("default");
            }
        });
    });*/

    /*/!* Change class for scroll *!/
    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            $('#menu').addClass("sticky2");
        }
        else {
            $('#menu').removeClass("sticky2");
        }
    });*/

    /* Gallery */
    if($('.lightbox-image').length) {
        $('.lightbox-image').fancybox({
            openEffect  : 'elastic',
            closeEffect : 'elastic',
            helpers : {
                media : {}
            }
        });
    }

    /* Title */
    $(".fancybox").fancybox({
        helpers : {
            title: {
                type: 'inside',
                position: 'bottom'
            }
        },
        nextEffect: 'fade',
        prevEffect: 'fade'
    });

    /* Button read more */
    $("#conent_hide").hide();
    $("#re").click(function(){
        x=$("#re").text();

        $("#conent_hide").slideToggle( function(){

            if(x == "Детальніше"){
                $("#re").text("Приховати").css({color:"red"});
            }
            else{
                $("#re").text("Детальніше").css({color:"green"});
            }
        });
    });

    /* Title Fancybox */
    $(document).ready(function () {
        $(".fancybox")
            .attr('rel', 'gallery')
            .fancybox({
                beforeLoad: function () {
                    var el, id = $(this.element).data('title-id');

                    if (id) {
                        el = $('#' + id);

                        if (el.length) {
                            this.title = el.html();
                        }
                    }
                },
                helpers: {
                    title: {
                        type: "inside"
                    }
                }
            });
    });

    /* Change item for gallery */
    class FilterGallery {
        constructor() {
            this.$filtermenuList = $('.filtermenu li input');
            this.$container      = $('.works_gallery');
            this.updateMenu('all');
            this.$filtermenuList.on('click', $.proxy(this.onClickFilterMenu, this));
        }
        onClickFilterMenu(event) {
            const $target      = $(event.target);
            const targetFilter = $target.data('filter');
            this.updateMenu(targetFilter);
            this.updateGallery(targetFilter);
        }
        updateMenu(targetFilter){
            this.$filtermenuList.removeClass('active');
            this.$filtermenuList.each((index, element)=>{
                const targetData = $(element).data('filter');
            if(targetData === targetFilter){
                $(element).addClass('active');
            }
        })
        }
        updateGallery(targetFilter) {
            if(targetFilter === 'all'){
                this.$container.fadeOut(300, ()=>{
                    $('.post').show();
                this.$container.fadeIn();
            });
            }else {
                this.$container.find('.post').each((index, element)=>{
                    this.$container.fadeOut(300, ()=>{
                    if($(element).hasClass(targetFilter)) {
                    $(element).show();
                }else {
                    $(element).hide();
                }
                this.$container.fadeIn();
            })
            });
            }
        }
    }
    const fliterGallery = new FilterGallery();


    /* Advertisement */
    $(document).ready(function () {
        var meetTheTeamCarousel = new MultiColumnCarousel();
        meetTheTeamCarousel.init();
    });

    var MultiColumnCarousel = function () {
        var that = this;
        this.multiColumnCarousel = $('.multi-column-carousel');
        this.viewport = this.multiColumnCarousel.find('.viewport');
        this.multiColumnButton = this.multiColumnCarousel.find('.multi-column-button');
        this.previousButton = this.multiColumnCarousel.find('.previous');
        this.nextButton = this.multiColumnCarousel.find('.next');
        this.paginationContainer = this.multiColumnCarousel.find('.pagination-container');
        this.pageContainer = this.multiColumnCarousel.find('.page-container');
        this.page = this.multiColumnCarousel.find('.page');

        this.viewportWidth = that.viewport.outerWidth();
        this.pageLength = this.page.length;
        this.currentPage = 0;
        this.animationDuration = 300;

        this.breakpoints = {
            large: {
                size: 1024,
                columns: 4
            },
            medium: {
                size: 960,
                columns: 3
            },
            small: {
                size: 667,
                columns: 1
            }
        };

        this.init = function () {
            this.setDimensions();
            this.hideAndDisplayButtons();
            this.generatePagination();

            this.multiColumnButton.on('click', this.handleMultiColumnButtonClick);

            $(window).resize(function () {
                that.viewportWidth = that.viewport.outerWidth();

                that.setDimensions();
                that.setToCurrentPage();
                that.generatePagination();
            });
        }

        this.setDimensions = function () {
            var columnsPerPage = this.getColumnsPerPage(),
                pageCount = this.pageLength / columnsPerPage,
                pageCountRounded = Math.ceil(pageCount),
                targetContainerWidth = that.viewportWidth * pageCountRounded,
                targetColumnWidth = this.viewportWidth / columnsPerPage;

            this.pageContainer.width(targetContainerWidth);
            this.page.width(targetColumnWidth);

            var currentTallestHeight = 0;
            this.page.height('auto');

            for (var i = 0; i < this.pageLength; i++) {
                var currentPageHeight = this.page.eq(i).outerHeight();
                if (currentPageHeight > currentTallestHeight) currentTallestHeight = currentPageHeight;
            }

            this.page.height(currentTallestHeight);
        }

        this.getColumnsPerPage = function () {
            var largeBreakpoint = this.breakpoints.large,
                mediumBreakpoint = this.breakpoints.medium,
                smallBreakpoint = this.breakpoints.small;

            if (this.viewportWidth > mediumBreakpoint.size) return largeBreakpoint.columns;
            if (this.viewportWidth <= mediumBreakpoint.size && this.viewportWidth > smallBreakpoint.size) return mediumBreakpoint.columns;
            if (this.viewportWidth <= smallBreakpoint.size) return smallBreakpoint.columns;
        }

        this.hideAndDisplayButtons = function () {
            var columnsPerPage = this.getColumnsPerPage();

            if(this.currentPage == 0) this.previousButton.addClass('hidden');
            if(this.currentPage + columnsPerPage >= this.pageLength) this.nextButton.addClass('hidden');

            if(this.currentPage > 0) this.previousButton.removeClass('hidden');
            if(this.currentPage + columnsPerPage < this.pageLength) this.nextButton.removeClass('hidden');
        }

        this.generatePagination = function () {
            var columnsPerPage = this.getColumnsPerPage(),
                pageCount = this.pageLength / columnsPerPage,
                pageCountRounded = Math.ceil(pageCount);

            this.paginationContainer.empty();

            if (pageCountRounded <= 1) return;

            for (var i = 0; i < pageCountRounded; i++) {
                var $indicator = $('<div>').addClass('indicator');
                $indicator.on('click', this.handleIndicatorClick);
                this.paginationContainer.append($indicator);
            }

            this.updatePagination();
        }

        this.handleIndicatorClick = function (e) {

            var $this = $(e.currentTarget),
                currentIndex = $this.index(),
                columnsPerPage = that.getColumnsPerPage(),
                $indicator = that.paginationContainer.find('.indicator');

            that.currentPage = currentIndex * columnsPerPage;

            $indicator.removeClass('active');
            $this.addClass('active');
            that.page.removeClass('active');
            that.page.eq(that.currentPage).addClass('active');

            that.pageContainer.animate({
                'left': -( that.multiColumnCarousel.find('.page.active').position().left )
            }, that.animationDuration);

            that.hideAndDisplayButtons();
        }

        this.updatePagination = function () {
            var columnsPerPage = this.getColumnsPerPage(),
                pageCount = this.pageLength / columnsPerPage,
                pageCountRounded = Math.ceil(pageCount);

            var $indicator = this.paginationContainer.find('.indicator'),
                currentSlide = (this.currentPage / (columnsPerPage / pageCountRounded)) / pageCountRounded;

            $indicator.removeClass('active');
            $indicator.eq(currentSlide).addClass('active');
        }

        this.handleMultiColumnButtonClick = function (e) {
            var $this = $(e.currentTarget),
                columnsPerPage = that.getColumnsPerPage();

            that.currentPage = that.multiColumnCarousel.find('.page.active').index();
            that.page.removeClass('active');

            if ($this.hasClass('next')) that.currentPage = that.currentPage + columnsPerPage;
            else that.currentPage = that.currentPage - columnsPerPage;

            that.page.eq(that.currentPage).addClass('active');
            that.pageContainer.animate({
                'left': -( that.multiColumnCarousel.find('.page.active').position().left )
            }, that.animationDuration);

            that.hideAndDisplayButtons();
            that.updatePagination();
        }

        this.setToCurrentPage = function () {
            var columnsPerPage = this.getColumnsPerPage();

            if (this.currentPage % columnsPerPage != 0) {
                this.currentPage--;

                that.page.removeClass('active');
                that.page.eq(that.currentPage).addClass('active');

                return this.setToCurrentPage();
            }

            that.pageContainer.css('left', -( that.multiColumnCarousel.find('.page.active').position().left ));
            that.hideAndDisplayButtons();
        }
    }

    /* Home Slider */
    if($('.main-slider.default-style .tp-banner').length){
        jQuery('.main-slider.default-style .tp-banner').show().revolution({
            delay:10000,
            startwidth:1200,
            startheight:720,
            hideThumbs:720,
            thumbWidth:80,
            thumbHeight:50,
            thumbAmount:5,
            navigationType:"bullet",
            navigationArrows:"0",
            navigationStyle:"preview3",
            touchenabled:"on",
            onHoverStop:"off",
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,
            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
            keyboardNavigation:"off",
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:40,
            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,
            shadow:0,
            fullWidth:"on",
            fullScreen:"off",
            spinner:"spinner4",
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",
            autoHeight:"off",
            forceFullWidth:"on",
            hideThumbsOnMobile:"on",
            hideNavDelayOnMobile:1500,
            hideBulletsOnMobile:"on",
            hideArrowsOnMobile:"on",
            hideThumbsUnderResolution:0,
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0,
            videoJsPath:"",
            fullScreenOffsetContainer: ""
        });
    }

    /* Scroll table */
    /*function horzScrollbarDetect() {
        var $scrollable = $('.scrollable')
        var $innerDiv = $('.scrollable > div');
        if ($innerDiv.outerWidth() < $innerDiv.get(0).scrollWidth) {
            $scrollable.addClass('is-scrollable');
            console.log('Scrollbar, WOOT!')
        } else {
            $scrollable.removeClass('is-scrollable');
            console.log('There is no scrollbar, only Zuul');
        }
    }
    $(document).ready(function() {
        horzScrollbarDetect();
        console.log('document. boom. ready.')
    });
    $(window).resize(function() {
        horzScrollbarDetect();
        console.log('window resized');
    });
*/
});
