<div class="license_bl">

    <div class="title_license">

        <h4><?php echo get_field('head_specialities_changes'); ?></h4>

        <p><?php echo get_field('for_license'); ?></p>

    </div>

    <h5><?php echo get_field('klass_1pointer'); ?></h5>

    <div class="scrollable">

        <div>

            <table border="1" cellspacing="0" cellpadding="0" class="first_tbl">

                <tbody>

                <tr>

                    <td class="all_spec">
                        <p><strong><?php pll_e('Шифр та найменування галузі знань'); ?></strong></p>
                    </td>

                    <td colspan="2" class="all_spec">
                        <p><strong><?php pll_e('Код та найменування спеціальності'); ?></strong></p>
                    </td>

                    <td class="all_spec">
                        <p><strong><?php pll_e('Ліцензований обсяг'); ?></strong></p>
                    </td>

                    <td class="all_spec">
                        <p><strong><?php pll_e('Номер і дата рішення'); ?></strong></p>
                    </td>

                </tr>

                <?php $preparation_magisters = get_field('students_repl'); ?>
                <?php if ($preparation_magisters): ?>

                    <!-- Title to the Preparation magisters -->
                    <tr>

                        <td colspan="5" class="head_td">
                            <p><strong><?php echo get_field('rep_magistrs'); ?></strong></p>
                        </td>

                    </tr>

                    <!-- Items (preparation magisters) -->
                    <?php
                    foreach (($preparation_magisters) as $students_repl) {
                        ?>

                        <tr>

                            <!-- Code and name of the knowledge -->
                            <td>
                                <p><?php echo $students_repl['cipher_knowledge']; ?></p>
                            </td>

                            <!-- Code of the speciality -->
                            <td>
                                <p><?php echo $students_repl['code_knowledge']; ?></p>
                            </td>

                            <!-- Name of the speciality -->
                            <td>
                                <p><?php echo $students_repl['name_knowledge']; ?></p>
                            </td>

                            <!-- Licensed volume -->
                            <td>
                                <p><?php echo $students_repl['licensed_volume']; ?></p>
                            </td>

                            <!-- Number and date of the decision -->
                            <td>
                                <p><?php echo $students_repl['number_decision']; ?></p>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                <?php endif; ?>

                <?php $repeater_doctors = get_field('repeater_doctors'); ?>
                <?php if ($repeater_doctors): ?>

                    <!-- Title to the Preparations to the doctors of philosophy -->
                    <tr>

                        <td colspan="5" class="head_td">
                            <p><strong><?php echo get_field('rep_doctors'); ?></strong></p>
                        </td>

                    </tr>

                    <!-- items (preparations to the doctors of philosophy) -->
                    <?php
                    foreach (($repeater_doctors) as $repeater_doctors) {
                        ?>

                        <tr>

                            <!-- Code and name of the knowledge -->
                            <td>
                                <p><?php echo $repeater_doctors['name_g']; ?></p>
                            </td>

                            <!-- Code of the speciality -->
                            <td>
                                <p><?php echo $repeater_doctors['code_sp']; ?></p>
                            </td>

                            <!-- Name of the speciality -->
                            <td>
                                <p><?php echo $repeater_doctors['name_of_spec']; ?></p>
                            </td>

                            <!-- Licensed volume -->
                            <td>
                                <p><?php echo $repeater_doctors['lic_obs']; ?></p>
                            </td>

                            <!-- Number and date of the decision -->
                            <td>
                                <p><?php echo $repeater_doctors['numdate']; ?></p>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                <?php endif; ?>

                <?php $preparations_bachelor = get_field('repeat_bakl'); ?>
                <?php if ($preparations_bachelor): ?>

                    <!-- Title to the Preparations of bachelor -->
                    <tr>

                        <td colspan="5" class="head_td">
                            <p><strong><?php echo get_field('head_ttl_bak'); ?></strong></p>
                        </td>

                    </tr>

                    <!-- Items (preparations of bachelor) -->
                    <?php
                    foreach (($preparations_bachelor) as $repeat_bakl) {
                        ?>

                        <tr>

                            <!-- Code and name of the knowledge -->
                            <td>
                                <p><?php echo $repeat_bakl['bak_sh']; ?></p>
                            </td>

                            <!-- Code of the speciality -->
                            <td>
                                <p><?php echo $repeat_bakl['code_spec_bakl']; ?></p>
                            </td>

                            <!-- Name of the speciality -->
                            <td>
                                <p><?php echo $repeat_bakl['name_spec_bakl']; ?></p>
                            </td>

                            <!-- Licensed volume -->
                            <td>
                                <p><?php echo $repeat_bakl['license_of_bakl']; ?></p>
                            </td>

                            <!-- Number and date of the decision -->
                            <td>
                                <p><?php echo $repeat_bakl['numberdate_bakl']; ?></p>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                <?php endif; ?>

                <?php $sm_spec = get_field('repeater_sm_spec'); ?>
                <?php if ($sm_spec): ?>

                    <!-- Title to the Preparations of junior specialists -->
                    <tr>

                        <td colspan="5" class="head_td">
                            <p><strong><?php echo get_field('head_title_sm_spec'); ?></strong></p>
                        </td>

                    </tr>

                    <!-- Items (preparations of junior specialists) -->
                    <?php
                    foreach (($sm_spec) as $repeater_sm_spec) {
                        ?>

                        <tr>

                            <!-- Code and name of the knowledge -->
                            <td>
                                <p><?php echo $repeater_sm_spec['educ_sm']; ?></p>
                            </td>

                            <!-- Code of the speciality -->
                            <td>
                                <p><?php echo $repeater_sm_spec['code_sm']; ?></p>
                            </td>

                            <!-- Name of the speciality -->
                            <td>
                                <p><?php echo $repeater_sm_spec['name_sm']; ?></p>
                            </td>

                            <!-- Licensed volume -->
                            <td>
                                <p><?php echo $repeater_sm_spec['license_sm']; ?></p>
                            </td>

                            <!-- Number and date of the decision -->
                            <td>
                                <p><?php echo $repeater_sm_spec['numer_date_sm']; ?></p>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                <?php endif; ?>

                </tbody>

            </table>

        </div>

    </div>

    <h5><?php echo get_field('class_2points'); ?></h5>

    <div class="scrollable">

        <div>

            <table border="1" cellspacing="0" cellpadding="0" class="second_tbl">

                <tbody>

                <tr>
                    <td rowspan="2" class="all_spec">
                        <p><strong><?php pll_e('Шифр та найменування галузі знань'); ?></strong></p>
                    </td>

                    <td rowspan="2" colspan="2" class="all_spec">
                        <p><strong><?php pll_e('Код та найменування спеціальності'); ?></strong></p>
                    </td>

                    <td colspan="2" class="all_spec">
                        <p><strong><?php pll_e('Ліцензований обсяг'); ?></strong></p>
                    </td>

                    <td rowspan="2" class="all_spec">
                        <p><strong><?php pll_e('Номер і дата рішення'); ?></strong></p>
                    </td>
                </tr>

                <tr>
                    <td class="all_spec">
                        <p><strong><?php pll_e('Денна'); ?></strong></p>
                    </td>

                    <td class="all_spec">
                        <p><strong><?php pll_e('Заочна'); ?></strong></p>
                    </td>
                </tr>

                <!-- Title to the Preparation magisters -->
                <?php $magistrs = get_field('repeater_magisters'); ?>
                <?php if ($magistrs): ?>

                    <tr>
                        <td colspan="6" class="head_td">
                            <p><strong><?php echo get_field('title_magisters_2'); ?></strong></p>
                        </td>
                    </tr>

                    <!-- Items (preparation magisters) -->
                    <?php
                    foreach (($magistrs) as $repeater_magisters) {
                        ?>

                        <tr>

                            <!-- Code and name of the knowledge -->
                            <td>
                                <p><?php echo $repeater_magisters['educ_res']; ?></p>
                            </td>

                            <!-- Code of the speciality -->
                            <td>
                                <p><?php echo $repeater_magisters['code_sp']; ?></p>
                            </td>

                            <!-- Name of the speciality -->
                            <td>
                                <p><?php echo $repeater_magisters['names_sp']; ?></p>
                            </td>

                            <!-- Licensed volume (full form) -->
                            <td>
                                <p><?php echo $repeater_magisters['day_form']; ?></p>
                            </td>

                            <!-- Licensed volume (part form) -->
                            <td>
                                <p><?php echo $repeater_magisters['part_form']; ?></p>
                            </td>

                            <!-- Number and date of the decision -->
                            <td>
                                <p><?php echo $repeater_magisters['date_numb']; ?></p>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                <?php endif; ?>

                <?php $specl = get_field('specials_repeater'); ?>
                <?php if ($specl): ?>

                    <!-- Title to the Preparation specialists -->
                    <tr>
                        <td colspan="6" class="head_td">
                            <p><strong><?php echo get_field('title_specialists_2'); ?></strong></p>
                        </td>
                    </tr>

                    <!-- Items (preparation specialists) -->
                    <?php
                    foreach (($specl) as $specials_repeater) {
                        ?>

                        <tr>

                            <!-- Code and name of the knowledge -->
                            <td>
                                <p><?php echo $specials_repeater['spec_sh']; ?></p>
                            </td>

                            <!-- Code of the speciality -->
                            <td>
                                <p><?php echo $specials_repeater['code_sp']; ?></p>
                            </td>

                            <!-- Name of the speciality -->
                            <td>
                                <p><?php echo $specials_repeater['code_sp']; ?></p>
                            </td>

                            <!-- Licensed volume (full form) -->
                            <td>
                                <p><?php echo $specials_repeater['full']; ?></p>
                            </td>

                            <!-- Licensed volume (part form) -->
                            <td>
                                <p><?php echo $specials_repeater['part']; ?></p>
                            </td>

                            <!-- Number and date of the decision -->
                            <td>
                                <p><?php echo $specials_repeater['date_numbrs']; ?></p>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                <?php endif; ?>

                <!-- Title to the Preparation of bachelor -->
                <?php $bachelor_tb = get_field('prepare_beh'); ?>
                <?php if ($bachelor_tb): ?>

                    <tr>
                        <td colspan="6" class="head_td">
                            <p><strong><?php echo get_field('beh_head_title'); ?></strong></p>
                        </td>
                    </tr>

                    <!-- Items (Preparation of bachelor) -->
                    <?php
                    foreach (($bachelor_tb) as $prepare_beh) {
                        ?>

                        <tr>

                            <!-- Code and name of the knowledge -->
                            <td>
                                <p><?php echo $prepare_beh['beh_shift']; ?></p>
                            </td>

                            <!-- Code of the speciality -->
                            <td>
                                <p><?php echo $prepare_beh['beh_code']; ?></p>
                            </td>

                            <!-- Name of the speciality -->
                            <td>
                                <p><?php echo $prepare_beh['beh_name_spec']; ?></p>
                            </td>

                            <!-- Licensed volume (full form) -->
                            <td>
                                <p><?php echo $prepare_beh['beh_full']; ?></p>
                            </td>

                            <!-- Licensed volume (part form) -->
                            <td>
                                <p><?php echo $prepare_beh['beh_part']; ?></p>
                            </td>

                            <!-- Number and date of the decision -->
                            <td>
                                <p><?php echo $prepare_beh['beh_date']; ?></p>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                <?php endif; ?>

                <!-- Title to the Preparation of junior specialists -->
                <?php $junior_spec = get_field('sml_spec_bl1'); ?>
                <?php if ($junior_spec): ?>

                    <tr>
                        <td colspan="6" class="head_td">
                            <p><strong><?php echo get_field('title_head_specials'); ?></strong></p>
                        </td>
                    </tr>

                    <!-- Items (Preparation of junior specialists) -->
                    <?php
                    foreach (($junior_spec) as $sml_spec_bl1) {
                        ?>

                        <tr>

                            <!-- Code and name of the knowledge -->
                            <td>
                                <p><?php echo $sml_spec_bl1['sml_bl1']; ?></p>
                            </td>

                            <!-- Code of the speciality -->
                            <td>
                                <p><?php echo $sml_spec_bl1['code_bl1']; ?></p>
                            </td>

                            <!-- Name of the speciality -->
                            <td>
                                <p><?php echo $sml_spec_bl1['namespec_bl1']; ?></p>
                            </td>

                            <!-- Licensed volume (full form) -->
                            <td>
                                <p><?php echo $sml_spec_bl1['bl1_full']; ?></p>
                            </td>

                            <!-- Licensed volume (part form) -->
                            <td>
                                <p><?php echo $sml_spec_bl1['bl1_part']; ?></p>
                            </td>

                            <!-- Number and date of the decision -->
                            <td>
                                <p><?php echo $sml_spec_bl1['datenum_bl1']; ?></p>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                    <!-- Items (Preparation of junior specialists) -->
                    <?php
                    foreach (get_field('repeater_specials2') as $repeater_specials2) {
                        ?>

                        <tr>

                            <!-- Name of the knowledge -->
                            <td colspan="3">
                                <p><?php echo $repeater_specials2['way_preparation']; ?></p>
                            </td>

                            <!-- License -->
                            <td colspan="2">
                                <p><?php echo $repeater_specials2['license_way']; ?></p>
                            </td>

                            <!-- Number and date of the decision -->
                            <td>
                                <p><?php echo $repeater_specials2['number_date_sps']; ?></p>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>

                <?php endif; ?>

                </tbody>

            </table>

        </div>

    </div>

    <?php $materials_links = get_field('materials_links'); ?>
    <?php if ($materials_links): ?>

        <!-- Materials with links -->
        <ul class="ul_a">

            <?php
            foreach (($materials_links) as $materials_links) {
                ?>

                <li><a target="_blank"
                       href="<?php echo $materials_links['links']; ?>"><?php echo $materials_links['name']; ?></a>
                </li>

                <?php
            }
            ?>

        </ul>

    <?php endif; ?>

    <?php $training_doctors = get_field('training_of_doctors'); ?>
    <?php if ($training_doctors): ?>

        <div class="doctors_learn">

            <h4><?php pll_e('Підготовка лікарів у БДМУ'); ?></h4>

            <!-- Content for Training of doctors -->
            <p><?php echo $training_doctors; ?></p>

        </div>

    <?php endif; ?>&nbsp;

    <?php $clinic_provisors = get_field('preparation_provisors'); ?>
    <?php if ($clinic_provisors): ?>

        <div class="doctors_learn">

            <h4><?php pll_e('Підготовка провізорів та клінічних провізорів в БДМУ'); ?></h4>

            <!-- Content for Clinic provisors -->
            <p><?php echo $clinic_provisors; ?></p>

        </div>

    <?php endif; ?>

    <?php $conclusions_of_expert = get_field('conclusions_of_expert'); ?>
    <?php if ($conclusions_of_expert): ?>

        <div class="expert_acrd">

            <h5><?php pll_e('Висновки експертних комісій про результати проведення акредитаційної експертизи'); ?>:</h5>

            <ul>

                <!-- Items (conclusions of expert) -->
                <?php
                foreach (($conclusions_of_expert) as $conclusions_of_expert) {
                    ?>

                    <li><a target="_blank"
                           href="<?php echo $conclusions_of_expert['link']; ?>"><?php echo $conclusions_of_expert['name']; ?></a>
                    </li>

                    <?php
                }
                ?>

            </ul>

        </div>

    <?php endif; ?>

    <?php $educational_programs = get_field('educational_programs'); ?>
    <?php if ($educational_programs): ?>

        <div class="expert_acrd">

            <h5><?php pll_e('Освітні програми'); ?>:</h5>

            <ul>

                <!-- Items (educational programs) -->
                <?php
                foreach (($educational_programs) as $educational_programs) {
                    ?>

                    <li><a target="_blank"
                           href="<?php echo $educational_programs['link']; ?>"><?php echo $educational_programs['name']; ?></a>
                    </li>

                    <?php
                }
                ?>

            </ul>

        </div>

    <?php endif; ?>

    <?php $informations_ch = get_field('informations'); ?>
    <?php if ($informations_ch): ?>

        <div class="expert_acrd acr">

            <h5><?php pll_e('Інформація про навчально-методичне та інформаційне забезпечення'); ?>:</h5>

            <ul>

                <!-- Items (information support) -->
                <?php
                foreach (($informations_ch) as $informations) {
                    ?>

                    <li><a target="_blank"
                           href="<?php echo $informations['link']; ?>"><?php echo $informations['name']; ?></a>
                    </li>

                    <?php
                }
                ?>

            </ul>

        </div>

    <?php endif; ?>

</div>