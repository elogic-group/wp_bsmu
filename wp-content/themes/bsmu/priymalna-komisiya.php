<?php
/*
  Template Name: Шаблон для "Приймальна комісія"
*/
?>

    <!-- Include Header -->
<?php get_header(); ?>

<?php the_post(); ?>


<?php if (get_field('main_image')): ?>

    <!-- Head image -->
    <div class="sub_header bg_1"
         style="background-image: linear-gradient(0deg, rgb(11, 37, 57),
                 rgba(3, 44, 75, 0.2)),
                 url(<?php the_field('main_image'); ?>)">

        <?php if (get_field('main_header')): ?>

            <div id="intro_txt" class="wow fadeInDown">
                <h1><?php the_field('main_header'); ?></h1>
                <p><?php echo get_field('main_header_title'); ?></p>
            </div>

        <?php endif; ?>

    </div>

<?php endif; ?>


    <!-- Preparatory department -->
    <div class="bg_page">

        <div class="line_container">

            <div class="container">

                <!-- Breadcrumbs -->
                <ul class="breadcrumbs_p">
                    <?php if (function_exists('bsmu_breadcrumbs')) bsmu_breadcrumbs(); ?>
                </ul>

            </div>

        </div>

        <div class="container">

            <div class="row_comisiya">

                <div class="memb_assoc">

                    <!-- Admission commission -->
                    <?php $admission = get_field('commision_university'); ?>
                    <?php if ($admission): ?>
                        <p><strong><?php pll_e('Приймальна комісія університету'); ?>: </strong><?php echo $admission; ?></p>
                    <?php endif; ?>

                    <!-- University administrative building -->
                    <?php $corps = get_field('corps_university'); ?>
                    <?php if ($corps): ?>
                        <p><strong><?php pll_e('Адміністративний корпус університету'); ?>: </strong><?php echo $corps; ?></p>
                    <?php endif; ?>

                    <!-- Map -->
                    <iframe src="<?php echo get_field('google_map_corps'); ?>" width="100%" height="480"></iframe>

                    <div class="docum">

                        <!-- Schedule for receiving documents -->
                        <?php $schedule = get_field('receiving_documents'); ?>
                        <?php if ($schedule): ?>

                            <h2><i class="fa fa-file-text-o"></i><?php pll_e('Графік прийому документів'); ?>:</h2>

                            <div class="row">

                                <!-- Items (Schedule for receiving documents) -->
                                <?php
                                foreach (($schedule) as $receiving_documents) {
                                    ?>

                                    <div class="col-md-6">

                                        <div class="docum_m">

                                            <div class="head_m">

                                                <!-- Working days -->
                                                <p><?php echo $receiving_documents['days']; ?></p>

                                            </div>

                                            <div class="content_m">

                                                <!-- Working hours -->
                                                <p><i class="fa fa-clock-o"
                                                      aria-hidden="true"></i><?php echo $receiving_documents['hours']; ?>
                                                </p>

                                                <!-- Lunch break -->
                                                <p><strong><?php pll_e('Обідня перерва'); ?>: </strong><?php echo $receiving_documents['lunch_break']; ?>
                                                </p>

                                            </div>

                                        </div>

                                    </div>

                                    <?php
                                }
                                ?>

                            </div>

                        <?php endif; ?>

                        <!-- Telephony -->
                        <?php $telephony = get_field('tel_sel_committee'); ?>
                        <?php if ($telephony): ?>

                            <h2><i class="fa fa-phone"></i><?php pll_e('Телефонія'); ?>:</h2>

                            <!-- Items -->
                            <?php
                            foreach (($telephony) as $tel_sel_committee) {
                                ?>

                                <!-- Name and phone number -->
                                <p><strong><?php echo $tel_sel_committee['name']; ?>: </strong><a
                                            href=""><?php echo $tel_sel_committee['phone_number']; ?></a></p>

                                <?php
                            }
                            ?>

                        <?php endif; ?>

                        <!-- Transfer from other educational institutions -->
                        <?php $others_vnz = get_field('tel_others_vnz'); ?>
                        <?php if ($others_vnz): ?>

                            <div class="for_others">

                                <p><strong><?php pll_e('Для звернень з приводу переводу з інших ВНЗ, деканати'); ?>:</strong></p>

                                <!-- Items -->
                                <?php
                                foreach (($others_vnz) as $tel_others_vnz) {
                                    ?>

                                    <!-- Name and phone number -->
                                    <p><?php echo $tel_others_vnz['name']; ?> - <a
                                                href="tel:<?php echo $tel_others_vnz['ph_number']; ?>"><?php echo $tel_others_vnz['ph_number']; ?></a>
                                    </p>

                                    <?php
                                }
                                ?>

                            </div>

                        <?php endif; ?>

                        <!-- Others phone number -->
                        <?php $ot_phones = get_field('others_ph_numbrs'); ?>
                        <?php if ($ot_phones): ?>

                            <div class="for_others">

                                <!-- Items -->
                                <?php
                                foreach (($ot_phones) as $others_ph_numbrs) {
                                    ?>

                                    <!-- Name and number -->
                                    <p><strong><?php echo $others_ph_numbrs['name']; ?>: </strong><a
                                                href="tel:<?php echo $others_ph_numbrs['ph_n']; ?>"><?php echo $others_ph_numbrs['ph_n']; ?></a>
                                    </p>

                                    <?php
                                }
                                ?>

                            </div>

                        <?php endif; ?>

                    </div>

                    <!-- Social network -->
                    <div class="soc_session">

                        <h2><i class="fa fa-bell-o"></i></i><?php pll_e('Ми у соціальних мережах'); ?>:</h2>

                        <ul>

                            <!-- Facebook -->
                            <?php $facebook_link = get_field('facebook_rel'); ?>
                            <?php if ($facebook_link): ?>
                                <li><a target="_blank" href="<?php echo $facebook_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png"
                                                title="Facebook" alt=""/></a></li>
                            <?php else: ?>
                                <li style="display: none;"><a target="_blank" href="<?php echo $facebook_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png"
                                                title="Facebook" alt=""/></a></li>
                            <?php endif; ?>

                            <!-- Wikipedia -->
                            <?php $wikipedia_link = get_field('wikipedia_rel'); ?>
                            <?php if ($wikipedia_link): ?>
                                <li><a target="_blank" href="<?php echo $wikipedia_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/wikipedia.png"
                                                title="Wikipedia" alt=""/></a></li>
                            <?php else: ?>
                                <li style="display: none;"><a target="_blank" href="<?php echo $wikipedia_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/wikipedia.png"
                                                title="Wikipedia" alt=""/></a></li>
                            <?php endif; ?>

                            <!-- News -->
                            <?php $news_link = get_field('news_rel'); ?>
                            <?php if ($news_link): ?>
                                <li><a href="<?php echo $news_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/press-delivery-service.png"
                                                title="<?php pll_e('Новини БДМУ'); ?>" alt=""/></a></li>
                            <?php else: ?>
                                <li style="display: none;"><a href="<?php echo $news_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/press-delivery-service.png"
                                                title="<?php pll_e('Новини БДМУ'); ?>" alt=""/></a></li>
                            <?php endif; ?>

                            <!-- Twitter -->
                            <?php $twitter_link = get_field('twitter_rel'); ?>
                            <?php if ($twitter_link): ?>
                                <li><a target="_blank" href="<?php echo $twitter_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png"
                                                title="Twitter" alt=""/></a></li>
                            <?php else: ?>
                                <li style="display: none;"><a target="_blank" href="<?php echo $twitter_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png"
                                                title="Twitter" alt=""/></a></li>
                            <?php endif; ?>

                            <!-- Youtube -->
                            <?php $youtube_link = get_field('youtube_rel'); ?>
                            <?php if ($youtube_link): ?>
                                <li><a target="_blank" href="<?php echo $youtube_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube.png"
                                                title="Youtube" alt=""/></a></li>
                            <?php else: ?>
                                <li style="display: none;"><a target="_blank" href="<?php echo $youtube_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube.png"
                                                title="Youtube" alt=""/></a></li>
                            <?php endif; ?>

                            <!-- Telegram -->
                            <?php $telegram_link = get_field('telegram_rel'); ?>
                            <?php if ($telegram_link): ?>
                                <li><a target="_blank" href="<?php echo $telegram_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/telegram.png"
                                                title="Telegram" alt=""/></a></li>
                            <?php else: ?>
                                <li style="display: none;"><a target="_blank" href="<?php echo $telegram_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/telegram.png"
                                                title="Telegram" alt=""/></a></li>
                            <?php endif; ?>

                            <!-- Email -->
                            <?php $mail_link = get_field('email_rel'); ?>
                            <?php if ($mail_link): ?>
                                <li><a href="mailto:<?php echo $mail_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/gmail.png"
                                                title="<?php pll_e('Написати листа...'); ?>" alt=""/></a></li>
                            <?php else: ?>
                                <li style="display: none;"><a href="mailto:<?php echo $mail_link; ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/gmail.png"
                                                title="<?php pll_e('Написати листа...'); ?>" alt=""/></a></li>
                            <?php endif; ?>

                        </ul>

                    </div>

                    <!-- Go to the site -->
                    <div class="committee_btn">

                        <a target="_blank" class="btn_select" href="<?php echo get_field('go_sitee'); ?>">
                            <?php pll_e('Перейти на сайт'); ?>
                        </a>

                    </div>

                </div>

            </div>

        </div>

    </div>


    <!-- Include Footer -->
<?php get_footer(); ?>