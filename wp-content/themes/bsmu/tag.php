<?php get_header(); ?>


    <section class="cs-blog-area">

        <div class="container">

            <div class="section-wrap">

                <div class="row row_blog-area">

                    <?php

                    $tag_slug = get_queried_object()->slug;

                    global $wp_query, $query_string;
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    $args = array(
                        'post_status' => 'publish',
                        'paged' => $paged,
                        'tag' => $tag_slug,
                        'lang' => pll_current_language(),
                        'posts_per_page' => 6
                    );
                    $posts = $wp_query->query($args);

                    foreach ($posts as $post) {
                        setup_postdata($post); ?>

                        <div class="col-md-4 col-sm-4">

                            <div class="cs-blog-col">
                                <div class="cs-blog-img-box news-inside">

                                    <!-- Image -->
                                    <?php
                                    if (get_the_ID() <= 26486) {
                                        $fimage = "https://www.bsmu.edu.ua/media/k2/galleries/" . get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) . "/1.JPG";
                                    } else {
                                        $fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
                                    }
                                    ?>

                                    <!-- Image -->
                                    <img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                                    <div class="cs-over-text">
                                        <ul class="cs-over-text-left">
                                            <li><i class="icon icon-Eye"></i><a
                                                        href=""><?php echo getPostViews(get_the_ID()); ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="cs-blog-content">
                                    <h4>
                                        <a href="<?php echo get_the_permalink(); ?>"><?php echo wp_trim_words(get_the_title(), 8); ?></a>
                                    </h4>
                                    <div class="content"><?php the_excerpt(); ?></div>

                                    <h5><?php echo get_the_date(); ?></h5>

                                    <a class="btn cs-btn-default hvr-curl-top-left"
                                       href="<?php echo get_the_permalink(); ?>"
                                       role="button"><?php pll_e('Детальніше'); ?></a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>

                </div>
            </div>

        </div>

        <div class="pagination">
            <?php my_pagenavi(); ?>
        </div>

    </section>

<?php get_footer(); ?>