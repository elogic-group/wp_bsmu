<!-- Include Header -->
<?php get_header();
the_post(); ?>



<div class="container">

    <div class="row">

        <div class="col-md-8">

            <!-- Actual news -->
            <section class="cs-blog-area blog_area">

                <div class="cs-blog-col">

                    <div class="cs-blog-img-box">

						<!-- Image -->
						<?php
							if (get_the_ID() <= 26486) {
								$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
							}else{
								$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
							}
						?>
						
						<!-- Image -->
						<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                        <div class="cs-over-text">

                            <ul class="cs-over-text-left">

                                <!-- Reviews -->
                                <li><i class="icon icon-Eye"></i><a href=""><?php setPostViews(get_the_ID()); ?><?php echo getPostViews(get_the_ID()); ?></a>
                                </li>
                            </ul>

                            <span class="post-date"><?php echo get_the_date(); ?></span>

                            <!--<ul class="cs-over-text-right"> -->

                                <!-- Likes -->
                                <?php// echo do_shortcode('[likebtn theme="heartcross" lang="uk" dislike_enabled="0" show_like_label="0" icon_dislike_show="0"]'); ?>

                            <!--</ul>-->

                        </div>

                    </div>

                    <div class="cs-blog-content">

                        <!-- Title -->
                        <h1><?php the_title(); ?></h1>

                        <!-- Author -->
                        <p class="cs-blog-dtlscmnt"><i class="fa fa-user"></i><?php the_author(); ?></p>

                        <!-- Quotes -->
                        <?php $quote = get_field('post_quote'); ?>
                        <?php if ($quote): ?>
                            <div class="quotes-blog">
                                <p><?php echo $quote; ?></p>
                            </div>
                        <?php endif; ?>


                        <!-- Content -->
                        <p>
                            <?php
                            if (get_the_ID() <= 26486) {
                                $gallery = get_post_galleries(get_the_ID(), false);
                                remove_shortcode('gallery');

                                echo preg_replace('/\[gallery ids="(.*)"\]/', '', get_the_content());
                            } else {
                                the_content();
                            }
                            ?>
                        </p>


                        <!-- Images -->
                        <?php $phot = get_field('post_photo'); ?>
                        <?php if ($phot) { ?>
                            <div class="row blog_photoes">

                                <?php
                                foreach (($phot) as $post_photo) {
                                    $photo_description = $post_photo['photo_description'];
                                    ?>

                                    <div class="col-md-6">
                                        <a class="fancybox" rel="gallery1" href="<?php echo $post_photo['photoes']; ?>"
                                           title="<?php if($photo_description){ echo $photo_description; } ?> ">
                                            <img src="<?php echo $post_photo['photoes']; ?>" alt=""/>
                                        </a>
                                    </div>

                                    <?php
                                }
                                ?>

                            </div>
                        <?php } elseif (get_the_ID() <= 26486 && $gallery) {
                            ?>
                            <div class="row blog_photoes">

                                <?php
                                foreach ($gallery[0]['src'] as $index => $post_photo) {
                                    ?>

                                    <div class="col-md-6">
                                        <a rel="gallery" data-title-id="title-1" href="https://www.bsmu.edu.ua/media/k2/galleries/<?php echo get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true); ?>/<?php echo $index + 1; ?>.JPG" class="lightbox-image fancybox" title="">
											<img class="k2img" src="https://www.bsmu.edu.ua/media/k2/galleries/<?php echo get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true); ?>/<?php echo $index + 1; ?>.JPG" alt=""/>
										</a>
                                    </div>

                                    <?php
                                }
                                ?>

                            </div>
                            <?php
                        } ?>


                        <!-- Tags block -->
                        <?php

                        echo '<div class="tags-block">';
                            $tags_word = pll_e('Позначки');
                            the_tags(''.$tags_word.':');
                        echo '</div>';

                        ?>



                        <div class="cs-blog-shareinfo">

                            <ul>

                                <li class="bshare-text"><?php pll_e('Поділитись'); ?>:</li>

                                <!-- Share Facebook -->
                                <li><a target="_blank"
                                       href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>"><i
                                                class="fab fa-facebook-f"></i></a></li>

                                <!-- Share GooglePlus -->
                                <li><a target="_blank"
                                       href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>"><i
                                                class="fab fa-google-plus-g"></i></a></li>

                                <!-- Share Linkedin -->
                                <li><a target="_blank"
                                       href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php the_title(); ?>&summary=&source="><i
                                                class="fab fa-linkedin-in"></i></a></li>

                                <!-- Share Telegram -->
                                <li><a target="_blank"
                                       href="tg://msg?text=<?php echo get_permalink(); ?>&title=<?php the_title(); ?>&summary=&source=">
                                        <i class="fab fa-telegram-plane"></i></i></a></li>

                                <!-- Share Viber -->
                                <li><a target="_blank"
                                       href="viber://forward?text=<?php echo get_permalink(); ?>&title=<?php the_title(); ?>&summary=&source=">
                                        <i class="fab fa-viber"></i></i></a></li>

                            </ul>


                        </div>

                    </div>

                </div>

            </section>

        </div>

        <div class="col-md-4 event_bl single">

            <div class="cs-section-tiltle">

                <!-- Title of the news -->
                <h3><?php pll_e('Актуальні новини'); ?></h3>

                <div class="cs-title-bdr-one"></div>

                <div class="cs-title-bdr-two"></div>

            </div>

            <!-- Post type = post(default) (max length = 3 posts) -->
            <?php

            $args = array(
                'numberposts' => 3,
                'category_name' => 'actual_events',
                'lang' => pll_current_language()
            );

            $posts = get_posts($args);

            foreach ($posts as $post) {
                setup_postdata($post); ?>

                <div class="cs-event-col cs-events-ccs">

                    <div class="cs-single-event">

                        <div class="cs-event-img1">

                            <div class="cs-inside-bdr">

								<!-- Image -->
								<?php
									if (get_the_ID() <= 26486) {
										$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
									}else{
										$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
									}
								?>
								
								<!-- Image -->
								<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                            </div>

                        </div>

                        <!-- Title with link to the post -->
                        <h4><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h4>

                        <!-- Description -->
                        <p><?php the_excerpt(); ?></p>

                        <!-- Tags block -->
                        <?php

                        echo '<div class="tags-block">';
                        $tags_word = pll_e('Позначки');
                        the_tags(''.$tags_word.':');
                        echo '</div>';

                        ?>

                    </div>

                </div>

            <?php }

            wp_reset_postdata();
            ?>

        </div>

    </div>

</div>


<!-- Include Footer -->
<?php get_footer(); ?>