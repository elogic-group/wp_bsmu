<?php
/*
  Template Name: Шаблон для "Головна сторінка"
*/
?>

<!-- Include Header -->
<?php get_header(); ?>

<!-- Include Slider Revolution Script -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/revolution.min.js"></script>

<!-- Slider Revolution -->
<section class="main-slider default-style style-one">

    <div class="tp-banner-container">

        <div class="tp-banner">

            <ul>

                <?php
                foreach (get_field('head_slider') as $head_slider) {
                    ?>

                    <li data-transition="curtain-1" class="" data-slotamount="1" data-masterspeed="1000"
                        data-thumb="<?php echo $head_slider['head_slide_image']; ?>" data-saveperformance="off"
                        data-title="Slide"><img src="<?php echo $head_slider['head_slide_image']; ?>" alt="Slide"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat"/>

                        <div class="tp-caption sfb sfb tp-resizeme"
                             data-x="30"
                             data-y="center"
                             data-hoffset="0"
                             data-voffset="-80"
                             data-speed="1500"
                             data-start="1000"
                             data-easing="easeOutBounce"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.3"
                             data-endspeed="1200"
                             data-endeasing="Power4.easeIn">
                            <h6 class="sub-title-1"><?php echo $head_slider['min_title_slider']; ?></h6>
                        </div>

                        <div class="tp-caption sfb sfb tp-resizeme"
                             data-x="30"
                             data-y="center"
                             data-hoffset="0"
                             data-voffset="0"
                             data-speed="1500"
                             data-start="1000"
                             data-easing="easeInOutQuart"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.3"
                             data-endspeed="1200"
                             data-endeasing="Power4.easeOut">
                            <h2 class="main-title-1"><?php echo $head_slider['title_slider']; ?></h2>
                        </div>

                        <div class="tp-caption sfb sfb tp-resizeme"
                             data-x="30"
                             data-y="center"
                             data-hoffset="0"
                             data-voffset="110"
                             data-speed="1500"
                             data-start="2000"
                             data-easing="easeOutExpo"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.3"
                             data-endspeed="1200"
                             data-endeasing="Power4.easeIn"><a href="<?php echo $head_slider['button_link_slider']; ?>"
                                                               class="btn cs-btn-default hvr-curl-top-left button2 slider-btn-2"><?php echo $head_slider['button_text_slider']; ?></a>
                        </div>

                    </li>

                    <?php
                }
                ?>

            </ul>

            <div class="tp-bannertimer"></div>

        </div>

    </div>

</section>
<!-- Advertisement -->
<div class="container">

    <div class="container_width">

        <!-- Post type = advertisement (max length = 3 posts) -->
        <?php

        $args = array(
            'numberposts' => 3,
            'post_type' => 'advertisement',
            'lang' => pll_current_language()
        );

        $posts = get_posts($args);

        foreach ($posts as $post) {
            setup_postdata($post); ?>

            <?php

            $date = get_field('advertisement_date');
            $today = date('d/m/Y');

            if ($date) {

                if ($today <= $date) { ?>

                    <div class="trips_bl adv_bl">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="course_list_desc">

                                    <!-- Title with link to the post -->
                                    <a href="<?php echo get_the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>

                                    <!-- Description -->
                                    <?php echo wp_trim_words(get_the_content(), 70); ?>

                                    <div class="det_infobutton">

                                        <!-- Link to the post -->
                                        <a href="<?php echo get_the_permalink(); ?>">
                                            <button><?php pll_e('Детальніше'); ?></button>
                                        </a>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                <?php

                }

                ?>

            <?php

            } else {

            ?>

                <div class="trips_bl adv_bl">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="course_list_desc">

                                <!-- Title with link to the post -->
                                <a href="<?php echo get_the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>

                                <!-- Description -->
                                <?php echo wp_trim_words(get_the_content(), 70); ?>

                                <div class="det_infobutton">

                                    <!-- Link to the post -->
                                    <a href="<?php echo get_the_permalink(); ?>">
                                        <button><?php pll_e('Детальніше'); ?></button>
                                    </a>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            <?php

            }

            ?>


        <?php }

        wp_reset_postdata();

        ?>

    </div>

</div>

<!-- Feautures of university -->
<div class="container container_hr">

    <div class="row">

        <div class="col-md-12 head_feautures">

            <!-- Head title -->
            <h2><?php echo get_field('header_university_benefits'); ?></h2>

            <!-- Title -->
            <h3><?php echo get_field('title_university_benefits'); ?></h3>

        </div>

    </div>

    <div class="row">

        <!-- Items -->
        <?php
        foreach (get_field('item_university_benefits') as $item_university_benefits) {
            ?>

            <div class="col-md-6">

                <div class="textwidget">

                    <div class="ar-recent-with-thumb">

                        <div class="media">

                            <div class="media-left">

                                <!-- Image -->
                                <div class="image-wrapper">

                                    <img src="<?php echo $item_university_benefits['image_benefit']; ?>"
                                         alt="Benefits"/>

                                </div>

                            </div>

                            <div class="media-body">

                                <!-- Title -->
                                <h4 class="media-heading"><?php echo $item_university_benefits['title_benefit']; ?></h4>

                                <!-- Description -->
                                <div class="content-desc"><?php echo $item_university_benefits['description_benefit']; ?></div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <?php
        }
        ?>

    </div>

    <hr>

</div>

<!-- Evets and news -->
<section class="cs-event-area">

    <div class="container">

        <div class="section-wrap">

            <div class="row animatedParent animateOnce">

                <div class="col-md-6">

                    <div class="cs-section-tiltle">

                        <!-- Title of the events -->
                        <h3><?php echo get_field('head_evens_title'); ?></h3>

                        <div class="cs-title-bdr-one"></div>

                        <div class="cs-title-bdr-two"></div>

                    </div>

                    <?php

                    // параметры по умолчанию
                    $args = array(
                        'numberposts' => 3,
                        'category_name' => 'events',
                        'lang' => pll_current_language()
                    );

                    $posts = get_posts($args);

                    foreach ($posts as $post) {
                        setup_postdata($post); ?>

                        <div class="cs-event-col cs-events-ccs">

                            <div class="cs-single-event wow fadeInRight" data-wow-delay="0.5s">

                                <div class="cs-event-img1">

                                    <div class="cs-inside-bdr">

                                        <!-- Image -->
                                        <?php
                                        if (get_the_ID() <= 26486 && get_post_galleries(get_the_ID(), false)) {
                                            $fimage = "https://www.bsmu.edu.ua/media/k2/galleries/" . get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) . "/1.JPG";
                                        } else {
                                            $fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
                                        }
                                        ?>

                                        <!-- Image -->
                                        <img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                                    </div>

                                </div>

                                <!-- Title with link to the post -->
                                <h4><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                <!-- Description -->
                                <p><?php the_excerpt(); ?></p>

                            </div>

                        </div>

                    <?php }

                    wp_reset_postdata(); // сброс

                    ?>

                </div>

                <div class="col-md-6">

                    <div class="cs-section-tiltle">

                        <!-- Title of the news -->
                        <h3><?php echo get_field('head_news_title'); ?></h3>

                        <div class="cs-title-bdr-one"></div>

                        <div class="cs-title-bdr-two"></div>

                    </div>

                    <?php

                    // параметры по умолчанию
                    $args = array(
                        'numberposts' => 3,
                        'category_name' => 'actual_events',
                        'lang' => pll_current_language()
                    );

                    $posts = get_posts($args);

                    foreach ($posts as $post) {
                        setup_postdata($post); ?>

                        <div class="cs-event-col cs-events-ccs">

                            <div class="cs-single-event wow fadeInRight" data-wow-delay="0.5s">

                                <div class="cs-event-img1">

                                    <div class="cs-inside-bdr">

                                        <?php
                                        if (get_the_ID() <= 26486 && get_post_galleries(get_the_ID(), false)) {
                                            $fimage = "https://www.bsmu.edu.ua/media/k2/galleries/" . get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) . "/1.JPG";
                                        } else {
                                            $fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
                                        }
                                        ?>

                                        <!-- Image -->
                                        <img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                                    </div>

                                </div>

                                <!-- Title with link to the post -->
                                <h4><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                <!-- Description -->
                                <p><?php the_excerpt(); ?></p>

                            </div>

                        </div>

                    <?php }

                    wp_reset_postdata(); // сброс

                    ?>

                </div>

            </div>

        </div>

    </div>

</section>

<!-- Gallery -->
<section class="cs-portfolio-area">

    <div class="section-title">

        <div class="row">

            <div class="col-sm-6 col-sm-offset-3">

                <div class="cs-section-tiltle cs-title-center cs-title-center-two">

                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/book.png" alt="Book"/>

                    <!-- Top title -->
                    <p class="cs-top-sub-title"><?php echo get_field('top_stud_title'); ?></p>

                    <!-- Title -->
                    <h2><span><?php echo get_field('title_stud'); ?></span></h2>

                    <div class="cs-title-bdr-one wp-one"></div>

                    <div class="cs-title-bdr-two wp-two"></div>

                    <!-- Description -->
                    <p class="title-r"><?php echo get_field('discribe_stud'); ?></p>

                </div>

            </div>

        </div>

    </div>

    <!-- Labels -->
    <ul class="filtermenu">

        <li>
            <input data-filter="all" type="radio" name="nav" id="one" checked="checked"/>
            <label for="one"><?php pll_e('Всі'); ?></label>
        </li>

        <li>
            <input data-filter="leisure" type="radio" name="nav" id="two"/>
            <label for="two"><?php pll_e('Дозвілля'); ?></label>
        </li>

        <li>
            <input data-filter="sport" type="radio" name="nav" id="three"/>
            <label for="three"><?php pll_e('Спорт'); ?></label>
        </li>

        <li>
            <input data-filter="blog" type="radio" name="nav" id="fourth"/>
            <label for="fourth"><?php pll_e('Блог'); ?></label>
        </li>

    </ul>

    <div class="works_gallery">

        <div class="container-fluid container-gallery">

            <div class="section-wrap">

                <div class="row">

                    <!-- Post type = leisure (max length = 8 posts) -->
                    <?php

                    $args = array(
                        'numberposts' => 4,
                        'category_name' => 'leisure',
                        'lang' => pll_current_language()
                    );

                    $posts = get_posts($args);

                    echo '<div class="post leisure">';

                    foreach ($posts as $post) {
                        setup_postdata($post); ?>

                        <div class="col-md-3 col-sm-3 pn">

                            <div class="cs-portfolio-col">

                                <div class="my-hover-9 my-hover-commmon">

                                    <div class="hover-img">

                                        <!-- Image -->
                                        <?php
                                        if (get_the_ID() <= 26486 && get_post_galleries(get_the_ID(), false)) {
                                            $fimage = "https://www.bsmu.edu.ua/media/k2/galleries/" . get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) . "/1.JPG";
                                        } else {
                                            $fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
                                        }
                                        ?>

                                        <!-- Image -->
                                        <img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                                    </div>

                                    <div class="infield">

                                        <h3>

                                            <!-- Image -->
                                            <a rel="gallery" data-title-id="title-1"
                                               href="<?php echo (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : '../wp-content/uploads/2017/11/news-default.jpg'; ?>"
                                               class="lightbox-image fancybox" title="">

                                                <!-- Button with link to the post -->
                                                <a href="<?php echo get_the_permalink(); ?>"><i
                                                            class="fa fa-eye" aria-hidden="true"></i></a>

                                                <!-- Title -->
                                                <p><?php echo mb_substr(strip_tags(get_the_title()), 0, 150); ?>
                                                    [...]</p>

                                            </a>

                                            <div id="title-1" class="hidden">

                                                <!-- Description -->
                                                <p><?php the_excerpt(); ?></p>

                                                <div class="btn_ttl">

                                                    <!-- Button with link to the post -->
                                                    <a href="<?php echo get_the_permalink(); ?>">
                                                        <button><?php pll_e('Детальніше'); ?></button>
                                                    </a>

                                                </div>

                                            </div>

                                        </h3>

                                    </div>

                                </div>

                            </div>

                        </div>

                    <?php }

                    echo '</div>';

                    wp_reset_postdata();

                    ?>

                    <!-- Post type = sport (max length = 8 posts) -->
                    <?php

                    $args = array(
                        'numberposts' => 4,
                        'category_name' => 'sport',
                        'lang' => pll_current_language()
                    );

                    $posts = get_posts($args);

                    echo '<div class="post sport">';

                    foreach ($posts as $post) {
                        setup_postdata($post); ?>

                        <div class="col-md-3 col-sm-3 col-xs-4 pn">

                            <div class="cs-portfolio-col">

                                <div class="my-hover-9 my-hover-commmon">

                                    <div class="hover-img">

                                        <!-- Image -->
                                        <?php
                                        if (get_the_ID() <= 26486 && get_post_galleries(get_the_ID(), false)) {
                                            $fimage = "https://www.bsmu.edu.ua/media/k2/galleries/" . get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) . "/1.JPG";
                                        } else {
                                            $fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
                                        }
                                        ?>

                                        <!-- Image -->
                                        <img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                                    </div>

                                    <div class="infield">

                                        <h3>

                                            <!-- Image -->
                                            <a rel="gallery" data-title-id="title-1"
                                               href="<?php echo (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : '../wp-content/uploads/2017/11/news-default.jpg'; ?>"
                                               class="lightbox-image fancybox" title="">

                                                <!-- Button with link to the post -->
                                                <a href="<?php echo get_the_permalink(); ?>"><i
                                                            class="fa fa-eye" aria-hidden="true"></i></a>

                                                <!-- Title -->
                                                <p><?php echo mb_substr(strip_tags(get_the_title()), 0, 150); ?>
                                                    [...]</p>

                                            </a>

                                            <div id="title-1" class="hidden">

                                                <!-- Description -->
                                                <p><?php the_excerpt(); ?></p>

                                                <div class="btn_ttl">

                                                    <!-- Button with link to the post -->
                                                    <a href="<?php echo get_the_permalink(); ?>">
                                                        <button><?php pll_e('Детальніше'); ?></button>
                                                    </a>

                                                </div>

                                            </div>

                                        </h3>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <!-- Post type = blog (max length = 8 posts) -->
                    <?php }

                    echo '</div>';

                    wp_reset_postdata();

                    ?>

                    <?php

                    $args = array(
                        'numberposts' => 4,
                        'category_name' => 'blogi',
                        'lang' => pll_current_language()
                    );

                    $posts = get_posts($args);

                    echo '<div class="post blog">';

                    foreach ($posts as $post) {
                        setup_postdata($post); ?>

                        <div class="col-md-3 col-sm-3 pn">

                            <div class="cs-portfolio-col">

                                <div class="my-hover-9 my-hover-commmon">

                                    <div class="hover-img">

                                        <!-- Image -->
                                        <?php
                                        if (get_the_ID() <= 26486 && get_post_galleries(get_the_ID(), false)) {
                                            $fimage = "https://www.bsmu.edu.ua/media/k2/galleries/" . get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) . "/1.JPG";
                                        } else {
                                            $fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
                                        }
                                        ?>

                                        <!-- Image -->
                                        <img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                                    </div>

                                    <div class="infield">

                                        <h3>

                                            <!-- Image -->
                                            <a rel="gallery" data-title-id="title-1"
                                               href="<?php echo (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : '../wp-content/uploads/2017/11/news-default.jpg'; ?>"
                                               class="lightbox-image fancybox" title="">

                                                <!-- Button with link to the post -->
                                                <a href="<?php echo get_the_permalink(); ?>"><i
                                                            class="fa fa-eye" aria-hidden="true"></i></a>

                                                <!-- Title -->
                                                <p><?php echo mb_substr(strip_tags(get_the_title()), 0, 150); ?>
                                                    [...]</p>

                                            </a>

                                            <div id="title-1" class="hidden">

                                                <!-- Description -->
                                                <p><?php the_excerpt(); ?></p>

                                                <div class="btn_ttl">

                                                    <!-- Button with link to the post -->
                                                    <a href="<?php echo get_the_permalink(); ?>">
                                                        <button><?php pll_e('Детальніше'); ?></button>
                                                    </a>

                                                </div>

                                            </div>

                                        </h3>

                                    </div>

                                </div>

                            </div>

                        </div>

                    <?php }

                    echo '</div>';

                    wp_reset_postdata();

                    ?>

                </div>

            </div>

        </div>

    </div>


</section>

<!-- Map -->
<div class="row contact_row">

    <div class="col-md-12">

        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2654.5949459462486!2d25.930043!3d48.291408!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc1d955925680022a!2z0JHRg9C60L7QstC40L3RgdC60LjQuSDQs9C-0YHRg9C00LDRgNGB0YLQstC10L3QvdGL0Lkg0LzQtdC00LjRhtC40L3RgdC60LjQuSDRg9C90LjQstC10YDRgdC40YLQtdGC!5e0!3m2!1sru!2sus!4v1508246108649"
                width="100%" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>

    </div>

</div>

<!-- Include Footer -->
<?php get_footer(); ?>
