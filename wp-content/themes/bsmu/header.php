<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png"/>
    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_template_directory_uri(); ?>/assets/css/revolution-slider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_template_directory_uri(); ?>/assets/css/jquery.fancybox.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_template_directory_uri(); ?>/assets/css/slick.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_template_directory_uri(); ?>/assets/css/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/responsive.css"/>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/slick.min.js"></script>
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/assets/js/revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js"></script>
    <!--    <script src="https://use.fontawesome.com/8d6d5efecf.js"></script>-->
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/readmore.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/app.js"></script>
</head>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<?php wp_head(); ?>

<script>
    var site_url = '<?php echo site_url(); ?>';
</script>

<body>

<div class="header-desktop-wrapper">

    <div class="top-header">

        <div class="container">

            <div class="row row_head">

                <div class="col-md-6 logo_states">
                    <?php echo the_custom_logo(); ?>
                    <p class="span_bsmu"><?php pll_e('Буковинський державний медичний університет'); ?></p>
                </div>

                <div class="col-md-6">

                    <div class="row">

                        <div class="col-md-11">

                            <?php get_search_form(); ?>

                            <div class="menu">

                                <nav class="nav_wrap nav_wrap_top">

                                    <?php wp_nav_menu('menu=top_menu&depth=3&menu_class=menu_kl'); ?>

                                </nav>

                            </div>

                        </div>

                        <div class="col-md-1">

                            <ul class="flags">

                                <?php pll_the_languages(array('show_flags' => 1, 'show_names' => 0)); ?>

                            </ul>


                        </div>

                    </div>

                </div>

            </div>


        </div>

    </div>
    <header>

        <div class="row row_mn default" id="menu">

            <div class="container">

                <div class="col-md-12 menu_col">

                    <div class="menu">

                        <nav class="nav_wrap">

                            <?php wp_nav_menu('menu=sticky_menu&depth=4&menu_class=menu_bg'); ?>

                        </nav>

                    </div>

                </div>

                <?php include "submenu.php"; ?>

            </div>

        </div>

    </header>

</div>

<div class="header-mobile-wrapper">

    <div class="top-header">

        <div class="container">

            <div class="row row_head">

                <div class="col-md-2 col-sm-2 col-xs-2 logo_states">
                    <?php echo the_custom_logo(); ?>
                </div>

                <div class="col-md-10 col-sm-10 col-xs-10 right-side">

                    <div class="row">

                        <div class="col-md-10 col-sm-10 col-xs-8">

                            <?php get_search_form(); ?>

                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-2">

                            <ul class="flags">

                                <?php pll_the_languages(array('show_flags' => 1, 'show_names' => 0)); ?>

                            </ul>


                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-2 mobile-btn-block">
                            <span class="mobile-nav-btn"><i class="fas fa-bars"></i></span>
                        </div>

                    </div>

                </div>

            </div>


        </div>

    </div>


    <header class="header-nav-mob">

        <div class="row row_mn default" id="menu">

            <div class="container">

                <div class="col-md-12 col-sm-12 col-xs-12 menu_col">

                    <div class="menu">

                        <nav class="nav_wrap">

                            <?php wp_nav_menu('menu=sticky_menu&depth=4&menu_class=menu_bg'); ?>

                            <?php wp_nav_menu('menu=top_menu&depth=3&menu_class=menu_kl'); ?>

                        </nav>

                    </div>

                </div>

            </div>

        </div>

    </header>

</div>