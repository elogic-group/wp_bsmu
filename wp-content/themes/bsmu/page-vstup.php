<!-- Vstup page -->
<div class="container vst_all">

    <h1><?php echo get_field('header'); ?></h1>

    <div class="row">

        <div class="col-md-3">

            <div class="edu_block">

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/graduation-hat-and-diploma.png"
                     alt=""/><br>

                <div class="counter" data-count="<?php echo get_field('number_of_years'); ?>">0</div>
                <span>+</span>

                <p><?php pll_e('Років славетної історії'); ?></p>

            </div>

        </div>

        <div class="col-md-3">

            <div class="edu_block">

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/multiple-users-silhouette.png" alt=""/><br>

                <div class="counter" data-count="<?php echo get_field('students_interns'); ?>">0</div>
                <span>+</span>

                <p><?php pll_e('Студентів, інтернів та слухачів'); ?></p>

            </div>

        </div>

        <div class="col-md-3">

            <div class="edu_block">

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/global.png" alt=""/><br>

                <div class="counter" data-count="<?php echo get_field('foreign_students'); ?>">0</div>
                <span>+</span>

                <p><?php pll_e('Іноземних студентів'); ?></p>

            </div>

        </div>

        <div class="col-md-3">

            <div class="edu_block">

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher-pointing-a-board-with-a-stick.png"
                     alt=""/><br>

                <div class="counter" data-count="<?php echo get_field('doctors_and_candidates'); ?>">0</div>
                <span>+</span>

                <p><?php pll_e('Докторів та кандидатів наук'); ?></p>

            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-md-3">

            <div class="edu_block">

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/3d-building.png" alt=""/><br>

                <div class="counter" data-count="<?php echo get_field('corresponds_and_clinical'); ?>">0</div>

                <p><?php pll_e('Корпусів та клінічних баз'); ?></p>

            </div>

        </div>

        <div class="col-md-3">

            <div class="edu_block">

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/home-front.png" alt=""/><br>

                <div class="counter" data-count="<?php echo get_field('guidelines'); ?>">0</div>

                <p><?php pll_e('Гуртожитків'); ?></p>

            </div>

        </div>

        <div class="col-md-3">

            <div class="edu_block">

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/trophy-shape.png" alt=""/><br>

                <div class="counter" data-count="<?php echo get_field('rating_of_western_region'); ?>">0</div>

                <p><?php pll_e('Місце в "Консолідованому рейтингу ВНЗ-2017" серед медичних ВНЗ західного регіону'); ?></p>

            </div>

        </div>

        <div class="col-md-3">

            <div class="edu_block">

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/trophy-shape.png" alt=""/><br>

                <div class="counter" data-count="<?php echo get_field('rating_of_informatyvity_of_web_sites'); ?>">0
                </div>

                <p><?php pll_e('Місце в рейтингу інформативності веб-сайтів медичних ВНЗ України'); ?></p>

            </div>

        </div>

    </div>

    <div class="rector_less">

        <?php the_content(); ?>

    </div>

</div>
