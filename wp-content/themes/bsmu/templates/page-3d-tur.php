<?php
/*
  Template Name: Шаблон для "3D тур (2)"
*/
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png"/>
    <title><?php if (is_home()) bloginfo('name'); else wp_title(''); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/3d.css"/>
</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div class="pulse"></div>
</div>

<section class="tour-section">

    <div class="full-container">

        <!-- Title -->
        <h1 id="intro_txt"><?php echo get_field('3d_name_university'); ?>
            <span><?php echo get_field('3d_subtitle'); ?></span></h1>

        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/scroll.png" class="img_cl" alt="Scroll"/>

    </div>

</section>

<!-- Items -->
<?php
foreach (get_field('tour_items') as $tour_items) {
    ?>

    <section class="tour-section">

        <div class="full-container">

            <!-- Image -->
            <div class="half-container" style="background-image: url(<?php echo $tour_items['tourd_photo']; ?>)"></div>

            <div class="half-container">

                <div class="txt_container">

                    <!-- Name -->
                    <h3><?php echo $tour_items['tourd_names']; ?></h3>

                    <!-- Description -->
                    <p><?php echo $tour_items['tourd_description']; ?></p>

                </div>

            </div>

        </div>

    </section>

    <?php
}
?>

<section class="tour-section">

    <div class="full-container">

        <!-- Default image -->
        <div class="half-container"
             style="background-image: url(<?php echo get_template_directory_uri(); ?>/wp-content/themes/bsmu/assets/img/back1.png);"></div>

        <div class="half-container">

            <div class="txt_container">

                <!-- Button with link to the Home page -->
                <a href="/" class="close_3d"><?php pll_e('Повернутися на головну'); ?></a>

            </div>

        </div>

    </div>

</section>

<!-- Next/prev buttons -->
<!--<nav>
    <ul class="vertical-nav">
        <li><a href="#0" class="prev">Попередній</a></li>
        <li><a href="#0" class="next">Наступний</a></li>
    </ul>
</nav>-->

<nav>
    <ul class="top-nav">
        <li><a href="#" class="back"><i class="arrow_back"></i></a></li>
    </ul>
</nav>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/3d.js"></script>
<script type='text/javascript'
        src='http://shtheme.com/demosd/atena/wp-content/themes/atena/js/functions.js?ver=4.8.3'></script>

</body>

</html>