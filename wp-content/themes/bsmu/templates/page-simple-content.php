<?php
/*
  Template Name: Шаблон для "simple content (1)"
*/
?>

<!-- Include Header -->
<?php get_header(); ?>

<?php the_post(); ?>


<?php if (get_field('main_image')): ?>

    <!-- Head image -->
    <div class="sub_header bg_1"
         style="background-image: linear-gradient(0deg, rgb(11, 37, 57),
                 rgba(3, 44, 75, 0.2)),
                 url(<?php the_field('main_image'); ?>)">

        <?php if (get_field('main_header')): ?>

            <div id="intro_txt" class="wow fadeInDown">
                <h1><?php the_field('main_header'); ?></h1>
                <p><?php echo get_field('main_header_title'); ?></p>
            </div>

        <?php endif; ?>

    </div>

<?php endif; ?>

<!-- Preparatory department -->
<div class="bg_page">

    <div class="line_container">

        <div class="container">

            <!-- Breadcrumbs -->
            <ul class="breadcrumbs_p">
                <?php if (function_exists('bsmu_breadcrumbs')) bsmu_breadcrumbs(); ?>
            </ul>

        </div>

    </div>

    <div class="container">

        <div class="row row_general">

            <?php if (is_page('1669')):
                //Вступ
                ?>

                <?php get_template_part('page-vstup'); ?>


            <?php elseif (is_page(array('2005', '2023'))): ?>

                <?php the_content(); ?>

                <?php if (get_field('outer_link')): ?>

                    <div class="outer_link_block">
                        <a href="<?php the_field('outer_link'); ?>" target="_blank"
                           class="outer_link"><?php pll_e('Перейти на сайт'); ?></a>
                    </div>

                <?php endif; ?>

            <?php else: ?>

                <div class="col-md-9">

                    <?php if (get_field('content_header')): ?>
                        <h2 class="main_header"><?php the_field('content_header'); ?></h2>
                    <?php endif; ?>

                    <div class="row">
                        <?php if (get_field('header')): ?>
                            <div class="col-md-12 head_focus">

                                <?php if (get_field('header')): ?>
                                    <h2><?php the_field('header'); ?></h2>
                                <?php endif; ?>

                                <?php if (get_field('subtitle')): ?>
                                    <h3><?php the_field('subtitle'); ?></h3>
                                <?php endif; ?>

                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="row row_focus">

                        <div class="col-md-4">

                            <div class="focus_on wow flipInX" data-wow-delay="1s">
                                <?php if (get_field('header_content_num1')): ?>
                                    <h2>01. <?php the_field('header_content_num1'); ?></h2>
                                <?php endif; ?>

                                <?php if (get_field('content_num1')): ?>
                                    <p><?php the_field('content_num1'); ?></p>
                                <?php endif; ?>
                            </div>

                            <div class="focus_on wow flipInX" data-wow-delay="2s">
                                <?php if (get_field('header_content_num3')): ?>
                                    <h2>03. <?php the_field('header_content_num3'); ?></h2>
                                <?php endif; ?>

                                <?php if (get_field('content_num3')): ?>
                                    <p><?php the_field('content_num3'); ?></p>
                                <?php endif; ?>
                            </div>

                        </div>

                        <div class="col-md-4">
                            <?php if (get_field('image')): ?>
                                <img src="<?php the_field('image'); ?>" alt="Focus" class="img_focus wow zoomIn"
                                     data-wow-delay="0.1s"/>
                            <?php endif; ?>
                        </div>

                        <div class="col-md-4">

                            <div class="focus_on wow flipInX" data-wow-delay="1.5s">
                                <?php if (get_field('header_content_num2')): ?>
                                    <h2>02. <?php the_field('header_content_num2'); ?></h2>
                                <?php endif; ?>

                                <?php if (get_field('content_num2')): ?>
                                    <p><?php the_field('content_num2'); ?></p>
                                <?php endif; ?>
                            </div>

                            <div class="focus_on wow flipInX" data-wow-delay="2.5s">
                                <?php if (get_field('header_content_num4')): ?>
                                    <h2>04. <?php the_field('header_content_num4'); ?></h2>
                                <?php endif; ?>

                                <?php if (get_field('content_num4')): ?>
                                    <p><?php the_field('content_num4'); ?></p>
                                <?php endif; ?>
                            </div>

                        </div>

                    </div>

                    <?php if (get_field('go_page')): ?>
                        <div class="focus_btn">
                            <a href="<?php the_field('go_page'); ?>"
                               class="outer_link"><?php the_field('link_name'); ?></a>
                        </div>
                    <?php endif; ?>

                    <?php if (get_field('go_link')): ?>
                        <div class="focus_btn">
                            <a href="<?php the_field('go_link'); ?>"
                               class="outer_link"><?php the_field('link_name'); ?></a>
                        </div>
                    <?php endif; ?>


                    <?php the_content(); ?>

                    <?php if (is_page('1059')):
                        //Ліцензія та акредитація
                        ?>

                        <?php get_template_part('litsenziya-ta-akreditatsiya'); ?>

                    <?php elseif (is_page('1658')):
                        //Членство в асоціаціях
                        ?>

                        <?php get_template_part('chlenstvo-v-asotsiatsiyah'); ?>

                    <?php elseif (is_page('1465')):
                        //Громадське обговорення
                        ?>

                        <?php get_template_part('kontakti-ta-rekviziti'); ?>

                    <?php elseif (is_page('2099')):
                        //Міжнародна діяльність
                        ?>

                        <?php get_template_part('mizhnarodna-diyalnist'); ?>

                    <?php endif; ?>

                    <?php if (get_field('outer_link')): ?>

                        <div class="outer_link_block">
                            <a href="<?php the_field('outer_link'); ?>" target="_blank"
                               class="outer_link"><?php pll_e('Перейти на сайт'); ?></a>
                        </div>

                    <?php endif; ?>

                </div>

                <div class="col-md-3 event_bl sidebar_events_news">

                    <!-- Include Sidebar -->
                    <?php get_template_part('sidebar'); ?>

                </div>

            <?php endif; ?>

        </div>

    </div>

</div>

<!-- Include Footer -->
<?php get_footer(); ?>
