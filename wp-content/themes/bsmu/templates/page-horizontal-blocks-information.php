<?php
/*
  Template Name: Шаблон для "horizontal blocks information (4)"
*/
?>

<!-- Include Header -->
<?php get_header(); ?>


<?php the_post(); ?>



<?php if (get_field('main_image')): ?>

    <!-- Head image -->
    <div class="sub_header bg_1"
         style="background-image: linear-gradient(0deg, rgb(11, 37, 57),
                 rgba(3, 44, 75, 0.2)),
                 url(<?php the_field('main_image'); ?>)">

        <?php if (get_field('main_header')): ?>

            <div id="intro_txt" class="wow fadeInDown">
                <h1><?php the_field('main_header'); ?></h1>
                <p><?php echo get_field('main_header_title'); ?></p>
            </div>

        <?php endif; ?>

    </div>

<?php endif; ?>

<div class="bg_page">

    <div class="line_container">

        <div class="container">

            <!-- Breadcrumbs -->
            <ul class="breadcrumbs_p">
                <?php if (function_exists('bsmu_breadcrumbs')) bsmu_breadcrumbs(); ?>
            </ul>

        </div>

    </div>

    <div class="container">

        <div class="row row_trips row_divis">

            <div class="col-md-9">


                <?php $the_content = get_the_content(); ?>
                <?php if ($the_content): ?>

                    <div class="row row_general">
                        <div class="col-md-12 info">
                            <?php the_content(); ?>
                        </div>
                    </div>

                <?php endif; ?>

                <!-- Items -->
                <?php $blocks_fields = get_field('repeater_with_content'); ?>
                <?php if ($blocks_fields): ?>
                    <?php
                    foreach (($blocks_fields) as $blocks) {
                        ?>

                        <div class="trips_bl">

                            <?php $header_block = $blocks['header_block']; ?>
                            <?php $header_block_url = $blocks['header_block_url']; ?>
                            <?php if ($header_block): ?>
                                <h2 class="horizontal_block_header">
                                    <a href="<?php echo $header_block_url; ?>"><?php echo $header_block; ?></a>
                                </h2>
                            <?php endif; ?>

                            <div class="row">

                                <div class="col-lg-4 col-md-4 col-sm-4">

                                    <div class="img_list">

                                        <!-- Image -->
                                        <?php $image = $blocks['image']; ?>
                                        <?php if ($image): ?>
                                            <img src="<?php echo $image; ?>" alt=""/>
                                        <?php else: ?>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/def-bsmu.jpg"
                                                 alt=""/>
                                        <?php endif; ?>

                                        <div class="short_info">

                                            <ul class="social_team icon_chp icon_deimagevisn items_soc">

                                                <!-- Facebook -->
                                                <?php $fb = $blocks['facebook']; ?>
                                                <?php if ($fb): ?>
                                                    <li><a target="_blank" href="<?php echo $fb; ?>"><i
                                                                    class="icon-facebook"></i></a></li>
                                                <?php endif; ?>

                                                <!-- Email -->
                                                <?php $mail = $blocks['email']; ?>
                                                <?php if ($mail): ?>
                                                    <li><a href="mailto:<?php echo $mail; ?>"
                                                           title="<?php echo $mail; ?>"><i class="icon-email"></i></a>
                                                    </li>
                                                <?php endif; ?>

                                            </ul>

                                        </div>

                                    </div>

                                </div>

                                <div class="clearfix visible-xs-block"></div>

                                <div class="col-lg-6 col-md-6 col-sm-6">

                                    <div class="course_list_desc">

                                        <?php $go_site = $blocks['go_site']; ?>
                                        <!-- Name -->

                                        <?php if ($go_site): ?>
                                        <a href="<?php echo $go_site; ?>">
                                        <?php endif; ?>

                                            <h3><?php echo $blocks['header_name']; ?></h3>

                                        <?php if ($go_site): ?>
                                        </a>
                                        <?php endif; ?>

                                        <?php $content_list = $blocks['content_list']; ?>

                                        <?php $content_element_repeater_top = $blocks['content_element_repeater_top']; ?>

                                        <?php if ($content_element_repeater_top): ?>
                                            <?php
                                            foreach (($content_element_repeater_top) as $element_content_top) {
                                                ?>

                                                <p>
                                                    <strong><?php echo $element_content_top['title']; ?>
                                                        - </strong><?php echo $element_content_top['text']; ?>
                                                </p>

                                                <?php
                                            }
                                            ?>
                                        <?php endif; ?>

                                        <?php $content = $blocks['content']; ?>
                                        <?php if ($content): ?>
                                            <p>
                                                <?php echo $content; ?>
                                            </p>
                                        <?php endif; ?>

                                        <?php if ($content_list): ?>
                                            <?php foreach (($content_list) as $element_list_content): ?>

                                                <p><strong><?php echo $element_list_content['header']; ?></strong></p>

                                                <ul>

                                                    <?php $list_element_content = $element_list_content['list_element_content']; ?>

                                                    <?php
                                                    foreach (($list_element_content) as $element_list) {
                                                        ?>

                                                        <li><span><?php echo $element_list['text']; ?></span></li>

                                                        <?php
                                                    }
                                                    ?>

                                                </ul>

                                            <?php endforeach; ?>
                                        <?php endif; ?>

                                        <?php $content_element_repeater = $blocks['content_element_repeater']; ?>



                                        <?php if ($content_list): ?>
                                            <?php
                                            foreach (($content_element_repeater) as $element_content) {
                                                ?>

                                                <p>
                                                    <strong><?php echo $element_content['title']; ?>
                                                        - </strong><?php echo $element_content['text']; ?>
                                                </p>

                                                <?php
                                            }
                                            ?>
                                        <?php endif; ?>

                                        <!-- Button with link to the site -->
                                        <?php if ($go_site): ?>
                                            <p class="go_site"><a target="_blank"
                                                                  href="<?php echo $go_site; ?>"><strong><?php pll_e('Перейти на сайт'); ?></strong></a>
                                            </p>
                                        <?php endif; ?>

                                    </div>

                                </div>

                                <?php $go_page = $blocks['go_page']; ?>

                                <?php if ($go_page): ?>

                                    <div class="col-lg-2 col-md-2 col-sm-2 border-left">

                                        <div class="details_list_col">

                                            <!-- Button with link to the personal page -->
                                            <div>

                                                <a href="<?php echo $go_page; ?>" target="_blank"
                                                   class="button_outline"><?php pll_e('Детальніше'); ?></a>

                                            </div>

                                        </div>

                                    </div>

                                <?php endif; ?>

                            </div>

                        </div>

                        <?php
                    }
                    ?>
                <?php endif; ?>

            </div>

            <div class="col-md-3 event_bl">

                <!-- Include Sidebar -->
                <?php get_template_part('sidebar'); ?>

            </div>

        </div>

    </div>

</div>

<!-- Include Footer -->
<?php get_footer(); ?>
