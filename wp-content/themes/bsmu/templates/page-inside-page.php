<?php
/*
  Template Name: Шаблон для "Внутрішня сторінка"
*/
?>

    <!-- Include Header -->
<?php get_header(); ?>


<?php the_post(); ?>


<?php if (get_field('main_image')): ?>

    <!-- Head image -->
    <div class="sub_header bg_1"
         style="background-image: linear-gradient(0deg, rgb(11, 37, 57),
                 rgba(3, 44, 75, 0.2)),
                 url(<?php the_field('main_image'); ?>)">

        <?php if (get_field('main_header')): ?>

            <div id="intro_txt" class="wow fadeInDown">
                <h1><?php the_field('main_header'); ?></h1>
                <p><?php echo get_field('main_header_title'); ?></p>
            </div>

        <?php endif; ?>

    </div>

<?php endif; ?>


    <div class="bg_page">

        <div class="line_container">

            <div class="container">

                <!-- Breadcrumbs -->
                <ul class="breadcrumbs_p">
                    <?php if (function_exists('bsmu_breadcrumbs')) bsmu_breadcrumbs(); ?>
                </ul>

            </div>

        </div>

        <div class="container">

            <div class="row row_rector">

                <div class="col-md-9">

                    <div class="facultss">

                        <?php the_content(); ?>

                        <?php if (get_field('outer_link')): ?>

                            <div class="outer_link_block center">
                                <br>
                                <hr>
                                <br>
                                <a href="<?php the_field('outer_link'); ?>" target="_blank"
                                   class="outer_link"><?php pll_e('Перейти на сайт'); ?></a>
                            </div>

                        <?php endif; ?>


                        <?php $block_header_card_info = get_field('block_header_card_info'); ?>

                        <?php if ($block_header_card_info): ?>

                            <?php foreach (($block_header_card_info) as $block_header_card_info): ?>

                                <div class="row info_card_row flex">

                                    <?php $header_info_card = $block_header_card_info['header_info']; ?>

                                    <?php if ($header_info_card): ?>
                                        <h5 class="header_info"><?php echo $header_info_card; ?></h5>
                                    <?php endif; ?>


                                    <?php $info_users = $block_header_card_info['info_user']; ?>

                                        <!-- Items -->
                                        <?php foreach (($info_users) as $info_user): ?>

                                            <div class="col-md-4 col-sm-6 col-xs-12">

                                                <div class="box_style_1">

                                                    <!-- Photo -->
                                                    <?php $photo_user = $info_user['photo']; ?>
                                                    <?php if ($photo_user): ?>
                                                        <p><img src="<?php echo $photo_user; ?>" class="img-circle styled"
                                                                alt=""/></p>
                                                    <?php else: ?>
                                                        <p>
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/noavatar1.png"
                                                                 class="img-circle styled" alt=""/></p>
                                                    <?php endif; ?>

                                                    <!-- User name and position -->
                                                    <h4><?php echo $info_user['username']; ?>
                                                        <?php
                                                            $position_user = $info_user['position'];
                                                            $degree_user = $info_user['degree'];
                                                        ?>
                                                        <?php if ($position_user): ?>
                                                            <p>
                                                                <small><?php echo $position_user; ?></small>
                                                            </p>
                                                        <?php endif; ?>
                                                        <?php if ($degree_user): ?>
                                                            <p>
                                                                <small><?php echo $degree_user; ?></small>
                                                            </p>
                                                        <?php endif; ?>
                                                    </h4>

                                                    <ul class="social_team">

                                                        <!-- Facebook -->
                                                        <?php $facebook_user = $info_user['facebook']; ?>
                                                        <?php if ($facebook_user): ?>
                                                            <li><a target="_blank" href="<?php echo $facebook_user; ?>" title="Facebook"><i
                                                                            class="icon-facebook"></i></a></li>
                                                        <?php endif; ?>

                                                        <!-- Twitter -->
                                                        <?php $twitter_user = $info_user['twitter']; ?>
                                                        <?php if ($twitter_user): ?>
                                                            <li><a target="_blank" href="<?php echo $twitter_user; ?>" title="Twitter"><i
                                                                            class="icon-twitter"></i></a></li>
                                                        <?php endif; ?>

                                                        <!-- Google+ -->
                                                        <?php $google_user = $info_user['google']; ?>
                                                        <?php if ($google_user): ?>
                                                            <li><a target="_blank" href="<?php echo $google_user; ?>" title="Google+"><i
                                                                            class="icon-google"></i></a></li>
                                                        <?php endif; ?>

                                                        <!-- ORCID -->
                                                        <?php $orcid_user = $info_user['orcid']; ?>
                                                        <?php if ($orcid_user): ?>
                                                            <li><a target="_blank" href="<?php echo $orcid_user; ?>" title="ORCID"><i
                                                                            class="icon-ORCID"></i></a></li>
                                                        <?php endif; ?>

                                                        <!-- ResearchGate -->
                                                        <?php $researchgate_user = $info_user['researchgate']; ?>
                                                        <?php if ($researchgate_user): ?>
                                                            <li><a target="_blank" href="<?php echo $researchgate_user; ?>" title="ResearchGate"><i
                                                                            class="icon-ResearchGate"></i></a></li>
                                                        <?php endif; ?>

                                                        <!-- Research ID -->
                                                        <?php $research_id_user = $info_user['research_id']; ?>
                                                        <?php if ($research_id_user): ?>
                                                            <li><a target="_blank" href="<?php echo $research_id_user; ?>" title="ResearchID"><i
                                                                            class="icon-researcherid"></i></a></li>
                                                        <?php endif; ?>

                                                        <!-- Linkedin -->
                                                        <?php $linkedin_user = $info_user['linkedin']; ?>
                                                        <?php if ($linkedin_user): ?>
                                                            <li><a target="_blank" href="<?php echo $linkedin_user; ?>" title="LinkedIn"><i
                                                                            class="icon-linkedin"></i></a></li>
                                                        <?php endif; ?>

                                                        <!-- Email -->
                                                        <?php $mail_user = $info_user['email']; ?>
                                                        <?php if ($mail_user): ?>
                                                            <li><a target="_blank" href="mailto:<?php echo $mail_user; ?>"
                                                                   title="<?php echo $mail_user; ?>"><i class="icon-email"></i></a>
                                                            </li>
                                                        <?php endif; ?>

                                                    </ul>

                                                    <hr>

                                                    <!-- Button with link to the profile -->
                                                    <?php $link_button = $info_user['profile']; ?>
                                                    <?php if ($link_button): ?>
                                                        <a href="<?php echo $link_button; ?>"
                                                           class="button_outlinee"><?php pll_e('Профіль'); ?></a>
                                                    <?php else: ?>
                                                        <a href="<?php echo $link_button; ?>" class="button_outlinee"
                                                           style="pointer-events: none; border: 2px solid #b6bfc4; color: #b6bfc4;"><?php pll_e('Профіль'); ?></a>
                                                    <?php endif; ?>

                                                </div>

                                            </div>

                                        <?php endforeach; ?>

                                </div>

                            <?php endforeach; ?>

                        <?php endif; ?>


                        <div class="row row_facultss_flex">

                            <div class="col-sm-6">

                                <?php $additional_info = get_field('additional_info'); ?>

                                <?php if ($additional_info): ?>

                                    <?php foreach (($additional_info) as $additional_info): ?>

                                        <p><span> <?php echo $additional_info['left_side']; ?>
                                                : </span><?php echo $additional_info['right_side']; ?></p>

                                    <?php endforeach; ?>

                                <?php endif; ?>

                            </div>

                            <div class="col-sm-6">

                                <?php $facebook_link = get_field('facebook_link'); ?>

                                <?php if ($facebook_link): ?>

                                    <ul class="social_team icon_chp">
                                        <li><a href="<?php echo $facebook_link; ?>"><i class="icon-facebook"></i></a>
                                        </li>
                                    </ul>

                                <?php endif; ?>

                            </div>

                        </div>

                        <?php $google_map = get_field('google_map'); ?>

                        <?php if ($google_map): ?>

                            <iframe src="<?php echo $google_map; ?>"
                                    width="100%" height="550" frameborder="0" style="border:0" allowfullscreen
                                    class="map_facults"></iframe>

                        <?php endif; ?>

                    </div>

                </div>

                <div class="col-md-3 event_bl">

                    <div class="cs-section-tiltle">

                        <!-- Title of the news -->
                        <h3><?php pll_e('Актуальні новини'); ?></h3>

                        <div class="cs-title-bdr-one"></div>

                        <div class="cs-title-bdr-two"></div>

                    </div>

                    <!-- Post type = post(default) (max length = 3 posts) -->
                    <?php

                    $taxonom_act = null;
                    if (get_connection(get_the_ID())) {
                        $taxonom_act = array(
                            array(
                                'taxonomy' => 'activity',
                                'terms' => get_connection(get_the_ID()),
                                'operator' => 'IN',
                                //'lang' => pll_current_language()
                            )
                        );
                    }

                    $args = array(
                        'numberposts' => 3,
                        'post_type' => 'post',
                        'tax_query' => $taxonom_act,
                        'lang' => pll_current_language()
//                        'category_name' => 'actual_events'
                    );
                    $posts = get_posts($args);

                    if (count($posts) == 0) {
                        $args = array(
                            'numberposts' => 3,
                            'post_type' => 'post',
                            'lang' => pll_current_language()
//                            'category_name' => 'actual_events'
                        );
                        $posts = get_posts($args);
                    }

                    foreach ($posts as $post) {
                        setup_postdata($post); ?>

                        <div class="cs-event-col cs-events-ccs">

                            <div class="cs-single-event">

                                <div class="cs-event-img1">

                                    <div class="cs-inside-bdr">

										<!-- Image -->
										<?php
											if (get_the_ID() <= 26486 && get_post_galleries(get_the_ID(), false)) {
												$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
											}else{
												$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
											}
										?>
										
										<!-- Image -->
										<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                                    </div>

                                </div>

                                <!-- Title with link to the post -->
                                <h4><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                <!-- Description -->
                                <p><?php the_excerpt(); ?></p>

                            </div>

                        </div>

                    <?php }

                    wp_reset_postdata();

                    ?>

                </div>

            </div>

        </div>

    </div>


<?php get_footer(); ?>