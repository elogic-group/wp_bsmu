<?php
/*
  Template Name: Шаблон для "information card (3)"
*/
?>

    <!-- Include Header -->
<?php get_header(); ?>

<?php the_post(); ?>


<?php if (get_field('main_image')): ?>

    <!-- Head image -->
    <div class="sub_header bg_1"
         style="background-image: linear-gradient(0deg, rgb(11, 37, 57),
                 rgba(3, 44, 75, 0.2)),
                 url(<?php the_field('main_image'); ?>)">

        <?php if (get_field('main_header')): ?>

            <div id="intro_txt" class="wow fadeInDown">
                <h1><?php the_field('main_header'); ?></h1>
                <p><?php echo get_field('main_header_title'); ?></p>
            </div>

        <?php endif; ?>

    </div>

<?php endif; ?>

    <div class="bg_page">

        <div class="line_container">

            <div class="container">

                <!-- Breadcrumbs -->
                <ul class="breadcrumbs_p">
                    <?php if (function_exists('bsmu_breadcrumbs')) bsmu_breadcrumbs(); ?>
                </ul>

            </div>

        </div>

        <?php $the_content = get_the_content(); ?>

        <?php if (is_page('2159')):
            //Студентське самоврядування
            ?>

            <?php get_template_part('studentske-samovryaduvannya') ?>

        <?php else: ?>

            <div class="container row_administration">

                <?php if ($the_content): ?>

                    <div class="row row_general">
                        <div class="col-md-12">
                            <div class="senate_inf">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>

                <?php $block_header_card_info = get_field('block_header_card_info'); ?>

                <?php if ($block_header_card_info): ?>

                    <?php foreach (($block_header_card_info) as $block_header_card_info): ?>

                        <div class="row info_card_row flex">

                            <?php $header_info_card = $block_header_card_info['header_info']; ?>

                            <?php if ($header_info_card): ?>
                                <h5 class="header_info"><?php echo $header_info_card; ?></h5>
                            <?php endif; ?>


                            <?php $info_users = $block_header_card_info['info_user']; ?>

                            <?php if (is_page('2170')): ?>
                            <div class="row row_kurators">
                                <?php endif; ?>

                                <!-- Items -->
                                <?php foreach (($info_users) as $info_user): ?>

                                    <div class="col-md-4 col-sm-6 col-xs-12">

                                        <div class="box_style_1">

                                            <?php if (is_page('2170')): ?>

                                            <?php else: ?>

                                                <!-- Photo -->
                                                <?php $photo_user = $info_user['photo']; ?>
                                                <?php if ($photo_user): ?>
                                                    <p><img src="<?php echo $photo_user; ?>" class="img-circle styled"
                                                            alt=""/></p>
                                                <?php else: ?>
                                                    <p>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/noavatar1.png"
                                                             class="img-circle styled" alt=""/></p>
                                                <?php endif; ?>

                                            <?php endif; ?>

                                            <?php if (is_page('2170')): ?>
                                                <?php $position_user = $info_user['position']; ?>
                                                <h2><?php echo $position_user; ?></h2>
                                                <h4><?php echo $info_user['username']; ?></h4>
                                                <p class="info"><?php echo $info_user['username']; ?></p>

                                            <?php else: ?>

                                                <!-- User name and position -->
                                                <h4><?php echo $info_user['username']; ?>
                                                    <?php $position_user = $info_user['position']; ?>
                                                    <?php if ($position_user): ?>
                                                        <p>
                                                            <small><?php echo $position_user; ?></small>
                                                        </p>
                                                    <?php endif; ?>
                                                </h4>

                                            <?php endif; ?>



                                            <?php if (is_page('2170')): ?>

                                            <?php else: ?>

                                                <!-- Short info about user -->
                                                <p class="shortinfo_user"><?php echo wp_trim_words($info_user['shortinfo'], 20); ?></p>

                                            <?php endif; ?>


                                            <ul class="social_team">

                                                <!-- Facebook -->
                                                <?php $facebook_user = $info_user['facebook']; ?>
                                                <?php if ($facebook_user): ?>
                                                    <li><a target="_blank" href="<?php echo $facebook_user; ?>" title="Facebook"><i
                                                                    class="icon-facebook"></i></a></li>
                                                <?php endif; ?>

                                                <!-- Twitter -->
                                                <?php $twitter_user = $info_user['twitter']; ?>
                                                <?php if ($twitter_user): ?>
                                                    <li><a target="_blank" href="<?php echo $twitter_user; ?>" title="Twitter"><i
                                                                    class="icon-twitter"></i></a></li>
                                                <?php endif; ?>

                                                <!-- Google+ -->
                                                <?php $google_user = $info_user['google']; ?>
                                                <?php if ($google_user): ?>
                                                    <li><a target="_blank" href="<?php echo $google_user; ?>" title="Google+"><i
                                                                    class="icon-google"></i></a></li>
                                                <?php endif; ?>

                                                <!-- ORCID -->
                                                <?php $orcid_user = $info_user['orcid']; ?>
                                                <?php if ($orcid_user): ?>
                                                    <li><a target="_blank" href="<?php echo $orcid_user; ?>" title="ORCID"><i
                                                                    class="icon-ORCID"></i></a></li>
                                                <?php endif; ?>

                                                <!-- ResearchGate -->
                                                <?php $researchgate_user = $info_user['researchgate']; ?>
                                                <?php if ($researchgate_user): ?>
                                                    <li><a target="_blank" href="<?php echo $researchgate_user; ?>" title="ResearchGate"><i
                                                                    class="icon-ResearchGate"></i></a></li>
                                                <?php endif; ?>

                                                <!-- Research ID -->
                                                <?php $research_id_user = $info_user['research_id']; ?>
                                                <?php if ($research_id_user): ?>
                                                    <li><a target="_blank" href="<?php echo $research_id_user; ?>" title="ResearchID"><i
                                                                    class="icon-researcherid"></i></a></li>
                                                <?php endif; ?>

                                                <!-- Linkedin -->
                                                <?php $linkedin_user = $info_user['linkedin']; ?>
                                                <?php if ($linkedin_user): ?>
                                                    <li><a target="_blank" href="<?php echo $linkedin_user; ?>" title="LinkedIn"><i
                                                                    class="icon-linkedin"></i></a></li>
                                                <?php endif; ?>

                                                <!-- Email -->
                                                <?php $mail_user = $info_user['email']; ?>
                                                <?php if ($mail_user): ?>
                                                    <li><a target="_blank" href="mailto:<?php echo $mail_user; ?>"
                                                           title="<?php echo $mail_user; ?>"><i class="icon-email"></i></a>
                                                    </li>
                                                <?php endif; ?>

                                            </ul>

                                            <?php if (is_page(array('2170', '624'))): ?>

                                            <?php else: ?>

                                                <hr>

                                                <!-- Button with link to the profile -->
                                                <?php $link_button = $info_user['profile']; ?>
                                                <?php if ($link_button): ?>
                                                    <a href="<?php echo $link_button; ?>"
                                                       class="button_outlinee"><?php pll_e('Профіль'); ?></a>
                                                <?php else: ?>
                                                    <a href="<?php echo $link_button; ?>" class="button_outlinee"
                                                       style="pointer-events: none; border: 2px solid #b6bfc4; color: #b6bfc4;"><?php pll_e('Профіль'); ?></a>
                                                <?php endif; ?>

                                            <?php endif; ?>

                                        </div>

                                    </div>

                                <?php endforeach; ?>

                                <?php if (is_page('2170')): ?>
                            </div>
                        <?php endif; ?>

                        </div>

                    <?php endforeach; ?>

                <?php endif; ?>

                <?php if (get_field('outer_link')): ?>

                    <div class="outer_link_block">
                        <br>
                        <hr>
                        <br>
                        <a href="<?php the_field('outer_link'); ?>" target="_blank"
                           class="outer_link"><?php pll_e('Перейти на сайт'); ?></a>
                    </div>

                <?php endif; ?>

            </div>


        <?php endif; ?>

    </div>

    <!-- Include Footer -->
<?php get_footer(); ?>