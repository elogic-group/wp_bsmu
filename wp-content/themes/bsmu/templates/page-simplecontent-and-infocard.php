<?php
/*
  Template Name: Шаблон для "simple content and information card (5)"
*/
?>

<!-- Include Header -->
<?php get_header(); ?>


<?php the_post(); ?>


<?php if (get_field('main_image')): ?>

    <!-- Head image -->
    <div class="sub_header bg_1"
         style="background-image: linear-gradient(0deg, rgb(11, 37, 57),
                 rgba(3, 44, 75, 0.2)),
                 url(<?php the_field('main_image'); ?>)">

        <?php if (get_field('main_header')): ?>

            <div id="intro_txt" class="wow fadeInDown">
                <h1><?php the_field('main_header'); ?></h1>
                <p><?php echo get_field('main_header_title'); ?></p>
            </div>

        <?php endif; ?>

    </div>

<?php endif; ?>

<div class="bg_page">

    <div class="line_container">

        <div class="container">

            <!-- Breadcrumbs -->
            <ul class="breadcrumbs_p">
                <?php if (function_exists('bsmu_breadcrumbs')) bsmu_breadcrumbs(); ?>
            </ul>

        </div>

    </div>

    <div class="container row_administration">

        <?php $library_page = is_page('926') ?>
        <div class="row row_general <?php if ($library_page): ?>row_library<?php endif; ?>">

            <div class="col-md-9">

                <div class="memb_assoc scientific_dep electr_archive">


                    <?php $the_content = get_the_content(); ?>
                    <?php if ($the_content): ?>
                        <div class="content-block">
                            <?php the_content(); ?>
                        </div>
                    <?php endif; ?>


                    <?php $alone_info_card = get_field('alone_info_card'); ?>

                    <?php if ($alone_info_card): ?>

                        <?php foreach (($alone_info_card) as $alone_info_card): ?>

                            <div class="row recp">

                                <div class="box_style_1">


                                    <?php $image = $alone_info_card['image']; ?>
                                    <?php if ($image): ?>
                                        <img class="img-circle styled" src="<?php echo $image; ?>" alt=""/>
                                    <?php else: ?>
                                        <img class="img-circle styled"
                                             src="<?php echo get_template_directory_uri(); ?>/assets/img/noavatar1.png"
                                             alt=""/>
                                    <?php endif; ?>


                                    <h4>
                                        <?php echo $alone_info_card['header']; ?>
                                    </h4>
                                    <p>
                                        <small>
                                            <?php echo $alone_info_card['description']; ?>
                                        </small>
                                    </p>


                                    <ul class="social_team">

                                        <?php $facebook = $alone_info_card['facebook']; ?>
                                        <?php if ($facebook): ?>

                                            <li><a href="<?php echo $facebook; ?>"><i class="icon-facebook"></i></a>
                                            </li>

                                        <?php endif; ?>

                                        <?php $email = $alone_info_card['email']; ?>
                                        <?php if ($email): ?>

                                            <li><a href="mailto:<?php echo $email; ?>"><i
                                                            class="icon-email"></i>
                                                </a></li>

                                        <?php endif; ?>
                                    </ul>

                                </div>

                                <?php $outer_link = $alone_info_card['outer_link']; ?>
                                <?php if ($outer_link): ?>

                                    <div class="outer_link_block center">
                                        <br>
                                        <a href="<?php echo $outer_link; ?>" target="_blank"
                                           class="outer_link"><?php pll_e('Перейти на сайт'); ?></a>
                                    </div>

                                <?php endif; ?>

                            </div>

                        <?php endforeach; ?>

                    <?php endif; ?>

                    <?php $block_header_card_info = get_field('block_header_card_info'); ?>

                    <?php if ($block_header_card_info): ?>

                    <?php foreach (($block_header_card_info) as $block_header_card_info): ?>

                    <div class="row">

                        <div class="col-md-12">

                            <?php $header_info_card = $block_header_card_info['header_info']; ?>

                            <?php if ($header_info_card): ?>
                                <h5 class="header_info"><?php echo $header_info_card; ?></h5>
                            <?php endif; ?>


                            <?php $info_users = $block_header_card_info['info_user']; ?>

                            <div class="row simple_infocard">

                                <!-- Items -->
                                <?php foreach (($info_users) as $info_user): ?>

                                <?php if (is_page('926')): ?>
                                <div class="col-md-6">
                                    <?php else: ?>
                                    <div class="col-md-4">
                                        <?php endif; ?>
                                        <div class="box_style_1 simple_infocard">

                                            <!-- Photo -->
                                            <?php $photo_user = $info_user['photo']; ?>
                                            <?php if ($photo_user): ?>
                                                <p><img src="<?php echo $photo_user; ?>"
                                                        class="img-circle styled"
                                                        alt=""/></p>
                                            <?php else: ?>
                                                <p>
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/noavatar1.png"
                                                         class="img-circle styled" alt=""/></p>
                                            <?php endif; ?>

                                            <?php if (is_page('926')): ?>
                                                <!-- User name and position -->
                                                <?php $position_user = $info_user['position']; ?>
                                                <h2><?php echo $info_user['username']; ?></h2>
                                                <h4><?php echo $position_user; ?></h4>

                                            <?php else: ?>

                                                <!-- User name and position -->
                                                <h4><?php echo $info_user['username']; ?>
                                                    <?php $position_user = $info_user['position']; ?>
                                                    <?php if ($position_user): ?>
                                                        <p>
                                                            <small><?php echo $position_user; ?></small>
                                                        </p>
                                                    <?php endif; ?>
                                                </h4>
                                            <?php endif; ?>


                                            <!-- Short info about user -->
                                            <?php if ($info_user['shortinfo']): ?>
                                                <p class="info_lib"><?php echo wp_trim_words($info_user['shortinfo'], 20); ?></p>
                                            <?php endif; ?>

                                            <?php $telephone_number = $info_user['telephone_number']; ?>
                                            <?php $telephone_number_link = $info_user['telephone_number_link']; ?>
                                            <?php if ($telephone_number): ?>
                                                <p><strong><?php pll_e('Номер телефону'); ?>: </strong><a
                                                            href="tel:<?php echo $telephone_number_link; ?>"><?php echo $telephone_number; ?></a>
                                                </p>
                                            <?php endif; ?>

                                            <?php $additional_field_info = $info_user['additional_field_info']; ?>
                                            <?php if ($additional_field_info): ?>
                                                <?php foreach (($additional_field_info) as $additional_field_info): ?>
                                                    <p>
                                                        <strong><?php echo $additional_field_info['left_side']; ?> </strong><?php echo $additional_field_info['right_side']; ?>
                                                    </p>
                                                <?php endforeach; ?>
                                            <?php endif; ?>

                                            <ul class="social_team">

                                                <!-- Facebook -->
                                                <?php $facebook_user = $info_user['facebook']; ?>
                                                <?php if ($facebook_user): ?>
                                                    <li><a target="_blank" href="<?php echo $facebook_user; ?>"><i
                                                                    class="icon-facebook"></i></a></li>
                                                <?php endif; ?>

                                                <!-- Twitter -->
                                                <?php $twitter_user = $info_user['twitter']; ?>
                                                <?php if ($twitter_user): ?>
                                                    <li><a target="_blank"
                                                           href="<?php echo $twitter_user; ?>"><i
                                                                    class="icon-twitter"></i></a></li>
                                                <?php endif; ?>

                                                <!-- Google+ -->
                                                <?php $google_user = $info_user['google']; ?>
                                                <?php if ($google_user): ?>
                                                    <li><a target="_blank" href="<?php echo $google_user; ?>"><i
                                                                    class="icon-google"></i></a></li>
                                                <?php endif; ?>

                                                <!-- Email -->
                                                <?php $mail_user = $info_user['email']; ?>
                                                <?php if ($mail_user): ?>
                                                    <li><a target="_blank"
                                                           href="mailto:<?php echo $mail_user; ?>"
                                                           title="<?php echo $mail_user; ?>"><i
                                                                    class="icon-email"></i></a>
                                                    </li>
                                                <?php endif; ?>

                                            </ul>

                                        </div>

                                    </div>

                                    <?php endforeach; ?>

                                </div>

                            </div>

                        </div>

                        <?php endforeach; ?>

                        <?php endif; ?>

                        <div class="library">

                            <?php $header_img_block = get_field('header_img_block'); ?>
                            <?php if ($header_img_block): ?>

                                <?php foreach (($header_img_block) as $header_img_block): ?>

                                    <div class="center_library">

                                        <h2><?php echo $header_img_block['header']; ?></h2>
                                        <img src="<?php echo $header_img_block['image']; ?>" alt="">

                                    </div>

                                <?php endforeach; ?>

                            <?php endif; ?>


                            <?php $header_img_info_block = get_field('header_img_info_block'); ?>
                            <?php if ($header_img_info_block): ?>

                            <?php foreach (($header_img_info_block) as $header_img_info_block): ?>

                                <div class="otd">

                                    <h2><?php echo $header_img_info_block['header']; ?></h2>

                                    <div class="row">

                                        <div class="col-md-6">

                                            <?php $telephone_number = $header_img_info_block['telephone_number']; ?>
                                            <?php $telephone_number_link = $header_img_info_block['telephone_number_link']; ?>
                                            <?php if ($telephone_number): ?>
                                                <p><strong><?php pll_e('Номер телефону'); ?>: </strong><a
                                                            href="tel:<?php echo $telephone_number_link; ?>"><?php echo $telephone_number; ?></a>
                                                </p>
                                            <?php endif; ?>

                                            <?php $info_field = $header_img_info_block['info_field']; ?>
                                            <?php if ($info_field): ?>
                                                <?php foreach (($info_field) as $info_field): ?>
                                                    <p>
                                                        <strong><?php echo $info_field['left_side']; ?> </strong><?php echo $info_field['right_side']; ?>
                                                    </p>
                                                <?php endforeach; ?>
                                            <?php endif; ?>

                                            <?php $text = $header_img_info_block['text']; ?>
                                            <?php if ($text): ?>
                                                <p><?php echo $text; ?></p>
                                            <?php endif; ?>

                                        </div>

                                        <div class="col-md-6">

                                            <!-- Photo -->
                                            <?php $image = $header_img_info_block['image']; ?>
                                            <?php if ($image): ?>
                                                <p><img src="<?php echo $image; ?>" alt=""/></p>
                                            <?php else: ?>
                                                <p>
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/def-bsmu.jpg"
                                                         class="img-circle styled" alt=""/></p>
                                            <?php endif; ?>

                                    </div>

                                </div>

                            </div>

                            <?php endforeach; ?>

                            <?php endif; ?>

                        </div>

                        <div class="develop_contacts">

                            <?php $telephone_with_symbols = get_field('telephone_with_symbols'); ?>
                            <?php if ($telephone_with_symbols): ?>
                                <p><i class="fa fa-phone"></i><strong> <?php pll_e('Номер телефону'); ?>: </strong><a
                                            href="tel:<?php get_field('telephone_without_symbols'); ?>">
                                        <?php echo $telephone_with_symbols; ?></a></p>
                            <?php endif; ?>

                            <?php $sector_address = get_field('sector_address'); ?>
                            <?php if ($sector_address): ?>
                                <p><i class="fa fa-map-marker"></i><strong> <?php pll_e('Поштова адреса сектору'); ?>
                                        : </strong><?php echo $sector_address; ?></p>
                            <?php endif; ?>

                            <?php $email_contacts = get_field('email_contacts'); ?>
                            <?php if ($email_contacts): ?>
                                <p><i class="fa fa-envelope"></i><strong> Email: </strong><a
                                            href="mailto:<?php echo $email_contacts; ?>"><?php echo $email_contacts; ?></a>
                                </p>
                            <?php endif; ?>

                        </div>

                        <?php $google_map = get_field('google_map'); ?>
                        <?php if ($google_map): ?>
                            <iframe src="<?php echo $google_map; ?>"
                                    width="100%" height="450" frameborder="0" style="border:0"
                                    allowfullscreen=""></iframe>
                        <?php endif; ?>


                        <?php if (get_field('outer_link')): ?>

                            <div class="outer_link_block center">
                                <br>
                                <br>
                                <a href="<?php the_field('outer_link'); ?>" target="_blank"
                                   class="outer_link"><?php pll_e('Перейти на сайт'); ?></a>
                            </div>

                        <?php endif; ?>


                    </div>

                </div>

                <div class="col-md-3 event_bl">

                    <!-- Include Sidebar -->
                    <?php get_template_part('sidebar'); ?>

                </div>

            </div>

        </div>

    </div>


    <!-- Include Footer -->
    <?php get_footer(); ?>

