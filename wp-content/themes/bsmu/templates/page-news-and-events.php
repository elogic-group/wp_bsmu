<?php
/*
  Template Name: Шаблон для "news and events (6)"
*/
?>

<!-- Include Header -->
<?php get_header(); ?>


<!-- Membership in associations -->

<div class="container">

    <div class="row">

        <div class="col-md-9">

            <div class="our-team">

                <?php $news_and_events_header = get_field('news_and_events_header'); ?>

                <?php if ($news_and_events_header): ?>

                    <h4><?php echo $news_and_events_header; ?></h4>

                <?php endif; ?>


                <div class="multi-column-carousel">

                    <div class="viewport">

                        <div id="actual-news" class="page-container actual-news-slider">

                            <?php

                            // параметры по умолчанию
                            $args = array(
                                'numberposts' => 9,
                                'category_name' => 'actual_events',
                                'lang' => pll_current_language()
                            );

                            $posts = get_posts( $args );

                            foreach($posts as $post){ setup_postdata($post);?>

                                <div class="page">

                                    <div class="page-content">

                                        <div class="head-shot">
                                            <!-- Photo -->
                                            <p>
												<!-- Image -->
												<?php
													if (get_the_ID() <= 26486) {
														$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
													}else{
														$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
													}
												?>
												
												<!-- Image -->
												<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>
											</p>
                                        </div>

                                        <a href="<?php echo get_the_permalink(); ?>">
                                            <p><?php echo wp_trim_words(get_the_title(), 20); ?></p>
                                        </a>

                                        <h5><?php echo get_the_date(); ?></h5>

                                    </div>

                                </div>

                            <?php }

                            wp_reset_postdata();

                            ?>

                        </div>

                    </div>

                </div>

            </div>


            <div class="row others_events">

                <div class="col-md-4">

                    <a href="<?php echo get_field('page_link_leisure') ?>" target="_blank">
                        <h4><?php pll_e('Дозвілля'); ?></h4></a>

                    <div class="header_blog">

                        <?php

                        // параметры по умолчанию
                        $args = array(
                            'numberposts' => 3,
                            'category_name' => 'leisure',
                            'lang' => pll_current_language()
                        );

                        $posts = get_posts( $args );

                        foreach($posts as $post){ setup_postdata($post);?>

                            <div class="bl_oth">

								<!-- Image -->
								<?php
									if (get_the_ID() <= 26486) {
										$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
									}else{
										$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
									}
								?>
								
								<!-- Image -->
								<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>
								
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <p><?php echo wp_trim_words(get_the_title(), 20); ?></p>
                                </a>
                                <h5><?php echo get_the_date(); ?></h5>
                            </div>

                        <?php }

                        wp_reset_postdata();

                        ?>

                        <div class="btn_ev">
                            <a href="<?php echo get_field('page_link_leisure') ?>" target="_blank"
                               class="outer_link"><?php pll_e('Більше...'); ?></a>
                        </div>

                    </div>

                </div>

                <div class="col-md-4">

                    <a href="<?php echo get_field('page_link_sport') ?>" target="_blank">
                        <h4><?php pll_e('Спорт'); ?></h4></a>

                    <div class="header_blog1">

                        <?php

                        // параметры по умолчанию
                        $args = array(
                            'numberposts' => 3,
                            'category_name' => 'sport',
                            'lang' => pll_current_language()
                        );

                        $posts = get_posts( $args );

                        foreach($posts as $post){ setup_postdata($post);?>

                            <div class="bl_oth">

								<!-- Image -->
								<?php
									if (get_the_ID() <= 26486) {
										$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
									}else{
										$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
									}
								?>
								
								<!-- Image -->
								<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>
								
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <p><?php echo wp_trim_words(get_the_title(), 20); ?></p>
                                </a>
                                <h5><?php echo get_the_date(); ?></h5>
                            </div>

                        <?php }

                        wp_reset_postdata();

                        ?>

                        <div class="btn_ev">
                            <a href="<?php echo get_field('page_link_sport') ?>" target="_blank"
                               class="outer_link"><?php pll_e('Більше...'); ?></a>
                        </div>

                    </div>

                </div>

                <div class="col-md-4">

                    <a href="<?php echo get_field('page_link_blog') ?>"><h4><?php pll_e('Блог'); ?></h4></a>

                    <div class="header_blog">

                        <?php

                        $args = array(
                            'numberposts' => 4,
                            'category_name' => 'blogi',
                            'lang' => pll_current_language()
                        );

                        $posts = get_posts( $args );

                        foreach($posts as $post){ setup_postdata($post);?>

                            <div class="bl_oth">

								<!-- Image -->
								<?php
									if (get_the_ID() <= 26486) {
										$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
									}else{
										$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
									}
								?>
								
								<!-- Image -->
								<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>
								
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <p><?php echo wp_trim_words(get_the_title(), 20); ?></p>
                                </a>
                                <h5><?php echo get_the_date(); ?></h5>
                            </div>

                        <?php }

                        wp_reset_postdata();

                        ?>

                        <div class="btn_ev">
                            <a href="<?php echo get_field('page_link_blog') ?>" target="_blank"
                               class="outer_link"><?php pll_e('Більше...'); ?></a>
                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="col-md-3 evnt">

            <a href="<?php echo get_field('page_link_events') ?>" target="_blank"><h4><?php pll_e('Події'); ?></h4></a>

            <!-- Post type = events (max length = 3 posts) -->
            <?php

            $args = array(
                'numberposts' => 3,
                'category_name' => 'events',
                'lang' => pll_current_language()
            );

            $posts = get_posts( $args );

            foreach($posts as $post){ setup_postdata($post);?>


                <div class="bl-events">

                    <div class="gallery_event">

                        <div class="gallery-image">


							<!-- Image -->
							<?php
								if (get_the_ID() <= 26486) {
									$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
								}else{
									$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
								}
							?>
							
							<!-- Image -->
							<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                        </div>

                    </div>

                    <div class="row n">

                        <div class="col-md-4">

                            <?php if($date){ ?>
                            <div class="data-events">
                                <h3><?php $date = get_post_meta($post->ID, 'data_events', true);
                                    if ($date != '') {
                                        echo date_i18n("j", strtotime($date));
                                    } ?></h3>
                                <p><?php $date = get_post_meta($post->ID, 'data_events', true);
                                    if ($date != '') {
                                        echo date_i18n("F", strtotime($date));
                                    } ?></p>
                                <span><?php echo get_field('time_events', $post->ID); ?></span>
                            </div>
                            <?php } ?>

                        </div>

                        <div class="col-md-8">

                            <p class="news"><a
                                        href="<?php echo get_the_permalink(); ?>"><?php echo wp_trim_words(get_the_title(), 20); ?></a>
                            </p>

                        </div>

                    </div>

                </div>

            <?php }

            wp_reset_postdata();

            ?>

            <div class="btn_ev">
                <a href="<?php echo get_field('page_link_events') ?>" target="_blank"
                   class="outer_link"><?php pll_e('Архів подій'); ?></a>
            </div>

        </div>


    </div>

</div>


<script type="text/javascript">
    jQuery(function ($) {
        $('.actual-news-slider').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 3
        });
    });
</script>

<!-- Include Footer -->
<?php get_footer(); ?>

