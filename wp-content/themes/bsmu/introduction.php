<?php
/*
  Template Name: Шаблон для "Вступ"
*/
?>

<!-- Include Header -->
<?php get_header(); ?>

<!-- Head image -->
<div class="sub_header bg_1"
     style="background-image: linear-gradient(0deg, rgb(11, 37, 57), rgba(3, 44, 75, 0.2)), url(<?php echo get_field('introduction_head_img'); ?>)">

    <div id="intro_txt" class="wow fadeInDown">

        <!-- Title -->
        <h1><?php echo get_field('introduction_title'); ?></h1>

        <!-- Subtitle -->
        <p><?php echo get_field('introduction_subtitle'); ?></p>

    </div>

</div>

<!-- Introduction -->
<div class="bg_page">

    <div class="line_container">

        <div class="container">

            <!-- Breadcrumbs -->
            <ul class="breadcrumbs_p">
                <?php if (function_exists('bsmu_breadcrumbs')) bsmu_breadcrumbs(); ?>
            </ul>

        </div>

    </div>

    <div class="container">

        <div class="row row_internship">

            <div class="col-md-12">

                <div class="intern">

                    <div class="row">

                        <div class="col-md-7">

                            <!-- Documents for submission -->
                            <?php $list_docs = get_field('list_documents'); ?>
                            <?php if ($list_docs): ?>

                                <h4><?php pll_e('Документи для подання'); ?>:</h4>

                                <ul>

                                    <!-- Items -->
                                    <?php
                                    foreach (($list_docs) as $list_documents) {
                                        ?>

                                        <!-- Documents -->
                                        <li><?php echo $list_documents['name']; ?></li>

                                        <?php
                                    }
                                    ?>

                                </ul>

                            <?php endif; ?>

                        </div>

                        <div class="col-md-5">

                            <!-- Image -->
                            <img src="<?php echo get_field('introduction_image'); ?>" alt=""/>

                        </div>

                    </div>

                    <!-- Beginning of filing documents -->
                    <p><strong><?php echo get_field('start_docs'); ?></strong></p>

                    <!-- Completion of submission of documents -->
                    <p><strong><?php echo get_field('end_docs'); ?></strong></p>

                    <!-- Content -->
                    <p class="introd_content"><?php echo get_field('introduction_content'); ?></p>

                    <!-- Documents -->
                    <?php $documents = get_field('introduction_links'); ?>
                    <?php if ($documents): ?>

                        <div class="intern_status">

                            <!-- Item -->
                            <?php
                            foreach (($documents) as $introduction_links) {
                                ?>

                                <!-- Name and link to the documents -->
                                <a target="_blank"
                                   href="<?php echo $introduction_links['link']; ?>"><?php echo $introduction_links['name']; ?></a>
                                <br>

                                <?php
                            }
                            ?>

                        </div>

                    <?php endif; ?>

                </div>

            </div>

        </div>

    </div>

</div>

<!-- Include Footer -->
<?php get_footer(); ?>
