<div class="contacts_rec">

    <div class="head_contacts">

        <h1><?php pll_e('Контакти та банківські реквізити'); ?></h1>

        <p><?php echo get_field('contacts_title'); ?></p>

        <!-- Phone number -->
        <?php $bsmu_tel = get_field('tel_bsmu'); ?>
        <?php if ($bsmu_tel): ?>
            <p><span><strong><?php pll_e('Телефон'); ?>:</strong> </span><a
                        href="tel:<?php echo $bsmu_tel; ?>"><?php echo $bsmu_tel; ?></a></p>
        <?php endif; ?>

        <!-- Fax -->
        <?php $bsmu_fax = get_field('fax_bsmu'); ?>
        <?php if ($bsmu_fax): ?>
            <p><span><strong><?php pll_e('Факс'); ?>:</strong> </span><a
                        href="tel:<?php echo $bsmu_fax; ?>"><?php echo $bsmu_fax; ?></a></p>
        <?php endif; ?>

        <!-- Email -->
        <?php $bsmu_mail = get_field('email_bsmu'); ?>
        <?php if ($bsmu_mail): ?>
            <p><span><strong>Email:</strong> </span><a
                        href="mailto:<?php echo $bsmu_mail; ?>"><?php echo $bsmu_mail; ?></a></p>
        <?php endif; ?>

    </div>

    <!-- Items of the Bank's requisites -->
    <?php
    foreach (get_field('bank_university') as $bank_university) {
        ?>

        <div class="payment_un">

            <!-- Payment for ... -->
            <h5><?php echo $bank_university['place_of_study']; ?></h5>

            <!-- Full name university -->
            <p><?php echo $bank_university['full_name_university']; ?></p>

            <!-- Address -->
            <p><?php echo $bank_university['address']; ?></p>

            <!-- Code -->
            <?php $code_edrpou = $bank_university['code_edrpou']; ?>
            <?php if ($code_edrpou): ?>
                <p><?php pll_e('Код за ЄДРПОУ'); ?>: <?php echo $code_edrpou; ?></p>
            <?php endif; ?>

            <!-- Account number -->
            <?php $check_n = $bank_university['number_check']; ?>
            <?php if ($check_n): ?>
                <h5><?php pll_e('р/р №'); ?><?php echo $check_n; ?></h5>
            <?php endif; ?>

            <!-- Recipient's bank -->
            <?php $recipient = $bank_university['recipient_bank']; ?>
            <?php if ($recipient): ?>
                <p><?php pll_e('Банк отримувача'); ?>: <?php echo $recipient; ?></p>
            <?php endif; ?>

            <!-- Purpose of payment -->
            <?php $purpose = $bank_university['purpose_of_payment']; ?>
            <?php if ($purpose): ?>
                <p><?php pll_e('Призначення платежу'); ?>: <?php echo $purpose; ?></p>
            <?php endif; ?>

        </div>

        <?php
    }
    ?>

    <!-- Map -->
    <iframe src="<?php echo get_field('chummery_google_maps'); ?>" width="100%" height="480"></iframe>

</div>