<div class="cs-section-tiltle">

    <!-- Title of the news -->
    <h3><?php pll_e('Актуальні новини'); ?></h3>

    <div class="cs-title-bdr-one"></div>

    <div class="cs-title-bdr-two"></div>

</div>

<!-- Post type = post(default) (max length = 3 posts) -->
<?php

$args = array(
    'numberposts' => 3,
    'category_name' => 'actual_events',
    'lang' => pll_current_language()
);

$posts = get_posts($args);

foreach ($posts as $post) {
    setup_postdata($post); ?>

    <div class="cs-event-col cs-events-ccs">

        <div class="cs-single-event">

            <div class="cs-event-img1">

                <div class="cs-inside-bdr">


						<!-- Image -->
						<?php
							if (get_the_ID() <= 26486) {
								$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
							}else{
								$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
							}
						?>
						
						<!-- Image -->
						<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

                </div>

            </div>

            <!-- Title with link to the post -->
            <h4><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h4>

            <!-- Description -->
            <p><?php the_excerpt(); ?></p>

            <!-- Tags block -->
            <?php

            echo '<div class="tags-block">';
            $tags_word = pll_e('Позначки');
            the_tags(''.$tags_word.':');
            echo '</div>';

            ?>

        </div>

    </div>

<?php }

wp_reset_postdata();

?>

<div class="cs-section-tiltle">

    <!-- Title of the events -->
    <h3><?php pll_e('Найближчі події'); ?></h3>

    <div class="cs-title-bdr-one"></div>

    <div class="cs-title-bdr-two"></div>

</div>

<!-- Post type = events (max length = 3 posts) -->
<?php

$args = array(
    'numberposts' => 3,
    'category_name' => 'events',
    'lang' => pll_current_language()
);

$posts = get_posts($args);

foreach ($posts as $post) {
    setup_postdata($post); ?>

    <div class="bl-events">

        <div class="gallery_event">

            <div class="gallery-image">


						<!-- Image -->
						<?php
							if (get_the_ID() <= 26486) {
								$fimage = "https://www.bsmu.edu.ua/media/k2/galleries/". get_post_meta(get_the_ID(), '_fgj2wp_old_k2_id', true) ."/1.JPG";
							}else{
								$fimage = (get_the_post_thumbnail_url(array())) ? get_the_post_thumbnail_url(array()) : site_url() . '/wp-content/uploads/2017/11/news-default.jpg';
							}
						?>
						
						<!-- Image -->
						<img class="k2img" src="<?php echo $fimage; ?>" alt=""/>

            </div>

        </div>

        <div class="row n">

            <div class="col-md-4">

                <!-- Date and time -->
                <div class="data-events">

                    <h3><?php $date = get_post_meta($post->ID, 'data_events', true);
                        if ($date != '') {
                            echo date_i18n("j", strtotime($date));
                        } ?></h3>

                    <p><?php $date = get_post_meta($post->ID, 'data_events', true);
                        if ($date != '') {
                            echo date_i18n("F", strtotime($date));
                        } ?></p>

                    <span><?php echo get_field('time_events', $post->ID); ?></span>

                </div>

            </div>

            <div class="col-md-12" style="background-color: transparent;">

                <!-- Title with link to the post -->
                <p class="news"><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></p>

                <!-- Description -->
                <p><?php the_excerpt(); ?></p>

                <!-- Tags block -->
                <?php

                echo '<div class="tags-block">';
                $tags_word = pll_e('Позначки');
                the_tags(''.$tags_word.':');
                echo '</div>';

                ?>

            </div>

        </div>

    </div>

<?php }

wp_reset_postdata();

?>