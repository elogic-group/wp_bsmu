<div class="memb_assoc">

    <?php $association_repeater = get_field('association_repeater'); ?>


    <?php foreach (($association_repeater) as $association_repeater ) : ?>

        <div class="payment_un">

            <h5> <?php echo $association_repeater['header']; ?></h5>

            <p><?php echo $association_repeater['content']; ?></p>

        </div>

    <?php endforeach ?>

</div>