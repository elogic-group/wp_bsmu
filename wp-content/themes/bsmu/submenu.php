<?php

if (has_children(get_the_ID()) && is_page()) {

    $args = array(
        'child_of' => get_the_ID()
    );
    $pages = get_pages($args);

    $my_wp_query = new WP_Query();
    $all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));
    $pgs = get_page_children(get_the_ID(), $all_wp_pages);

    ?>

    <div class="row menu_general_all submenu">

        <div class="col-md-2 header-block">

            <h2 style="margin-bottom: 15px;"><a><?php echo $post->post_title ?></a></h2>

            <i class="fa fa-angle-right" aria-hidden="true" style="font-size: 37px;"></i>

        </div>

        <div class="col-md-10">

            <div class="menu all_g">

                <nav class="nav_wrap">

                    <ul class="menu1">

                        <?php
                        wp_list_pages(array(
                            'child_of' => get_the_ID(),
                            'depth' => 2,
                            'sort_order' => 'asc',
                            'title_li' => ''
                        ));
                        ?>

                    </ul>

                </nav>

            </div>

        </div>

    </div>

<?php } ?>