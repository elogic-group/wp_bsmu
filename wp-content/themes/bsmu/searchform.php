<form role="search" method="get" id="searchform" action="<?php echo home_url('/') ?>">
    <div class="box">
        <div class="container-3">
            <input id="search" type="text" value="<?php echo get_search_query() ?>" placeholder="<?php pll_e('Пошук...'); ?>" name="s"
                   id="s"/>
            <button class="icon-search-btn">
                <span class="icon"><i class="fa fa-search"></i></span>
            </button>
        </div>
        <span class="icon trigger-form-mobile"><i class="fa fa-search"></i></span>
    </div>
</form>